package com.lysen.interceptor;

import com.lysen.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings({"all"})
/**
 * 每次访问都获取ip地址，统计
 */
@Component
public class UniqueVisitorInterceptor implements HandlerInterceptor {
    @Autowired
    private DataService dataService;

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler
    ) throws Exception {
        String ip = request.getRemoteHost();
        dataService.addUniqueVisitor(ip);
        return true;
    }
}
