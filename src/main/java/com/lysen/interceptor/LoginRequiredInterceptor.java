package com.lysen.interceptor;

import com.lysen.annotation.LoginRequired;
import com.lysen.exception.LogoutException;
import com.lysen.session.RedisSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

@SuppressWarnings({"all"})
//@Component
public class LoginRequiredInterceptor implements HandlerInterceptor {
    @Autowired
    private RedisTemplate redisTemplate;
    @Value("${mine.rsession.lifetime}")
    protected Long lifetime;

    /**
     * handler的类型————
     * 拦截到静态资源，handler类型为org.springframework.web.servlet.resource.ResourceHttpRequestHandler
     * 拦截到Controller方法，handler类型为org.springframework.web.method.HandlerMethod
     */
    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response, Object handler
    ) throws Exception {
        //拦截到Controller方法
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            //获取调用的Controller方法
            Method method = handlerMethod.getMethod();
            //有 @LoginRequired 注解
            if (method.getAnnotation(LoginRequired.class) != null) {
                //如果没有登录抛出异常
                if (RedisSession.getRedisSession(
                        request,
                        response,
                        redisTemplate,
                        lifetime
                ).getAttr("user") == null) {
                    throw new LogoutException();
                }
            }
        }
        return true;
    }
}
