package com.lysen.service;

import java.time.LocalDate;

@SuppressWarnings({"all"})
/**
 * 统计有关的业务层
 */
public interface DataService {
    /**
     * 用户访问
     * @param ip
     */
    void addUniqueVisitor(String ip);

    /**
     * 某段时间用户访问量
     * @return
     */
    long countUniqueVisitor(LocalDate start, LocalDate end);

    /**
     * 新增日活跃用户
     * @param ip
     */
    void addDailyActiveUser(Long userId);

    /**
     * 某段时间活跃用户数
     * @return
     */
    long countDailyActiveUser(LocalDate start, LocalDate end);

}
