package com.lysen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lysen.common.PagingResult;
import com.lysen.common.QueryParam;
import com.lysen.model.entity.Message;
import com.lysen.model.vo.Conversation;

import java.util.List;

@SuppressWarnings({"all"})
public interface MessageService extends IService<Message> {
    /**
     * 查询某个用户的未读消息数
     */
    Long selectUnreadCount(Long id);

    /**
     * 查询某个用户的会话列表
     */
    PagingResult<Conversation> selectConversationList(QueryParam<Long> queryParam);

    /**
     * 查询某个用户未读系统通知数
     */
    Long selectUnreadCountOfSystemMessage(Long id);

    /**
     * 查询某个用户系统通知的会话列表
     */
    List<Conversation> selectConversationListOfSystemMessage(Long id);
}
