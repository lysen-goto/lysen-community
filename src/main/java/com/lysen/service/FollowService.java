package com.lysen.service;

import com.lysen.common.PagingResult;
import com.lysen.common.QueryParam;
import com.lysen.model.entity.User;
import com.lysen.model.dto.FollowDto;

@SuppressWarnings({"all"})
public interface FollowService {
    /**
     * 粉丝列表的redisKey
     */
    default String followerRedisKey(FollowDto param) {
        return "follower:" + param.getFolloweeTypeCode() + ":" + param.getFolloweeId();
    }

    /**
     * 关注列表
     */
    default String followeeRedisKey(FollowDto param) {
        return "followee:" + param.getFolloweeTypeCode() + ":" + param.getFollowerId();
    }


    /**
     * 关注
     */
    void follow(FollowDto param);

    /**
     * 取消关注
     */
    void cancelFollow(FollowDto param);

    /**
     * 粉丝数目
     */
    Long countOfFollower(FollowDto param);

    /**
     * 关注数目
     */
    Long countOfFollowee(FollowDto param);

    /**
     * 是否关注
     */
    boolean isFollow(FollowDto param);

    /**
     * 获取粉丝列表
     */
    PagingResult<User> followerList(QueryParam<FollowDto> queryParam);

    /**
     * 获取关注列表
     */
    PagingResult followeeList(QueryParam<FollowDto> queryParam);
}
