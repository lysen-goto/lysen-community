package com.lysen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lysen.model.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

@SuppressWarnings({"all"})
@Transactional
public interface UserService extends IService<User> {
}
