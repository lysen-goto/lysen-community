package com.lysen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lysen.common.PagingResult;
import com.lysen.common.QueryParam;
import com.lysen.model.entity.Comment;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SuppressWarnings({"all"})
public interface CommentService extends IService<Comment> {
    /**
     * 查询某个帖子的评论（分页）
     * @param id 帖子id
     */
    PagingResult<Comment> selectForDiscussPost(QueryParam<Long> queryParam);

    /**
     * 查询某个评论的评论（分页）
     * @param id 评论id
     */
    List<Comment> selectForComment(Long id);

    /**
     * 新增帖子，开启事务
     */
    @Transactional
    Comment insert(Comment comment);

    /**
     * 获取评论所在的帖子的id
     */
    Long postIdOf(Long commentId);
}
