package com.lysen.service;


@SuppressWarnings({"all"})
public interface LikeService {
    /**
     * 获取某个帖子的点赞数
     */
    Long likeCountOfPost(Long discussPostId);

    /**
     * 获取某条评论的点赞数
     */
    Long likeCountOfComment(Long commentId);

    /**
     * 用户的获赞数
     */
    Integer likeCountOfUser(Long userId);

    /**
     * 点赞帖子
     */
    void likePost(Long userId, Long discussPostId);

    /**
     * 取消点赞帖子
     */
    void cancelLikePost(Long userId, Long discussPostId);

    /**
     * 如果为已点赞，取消点赞后返回false
     * 如果为未点赞，点赞后返回true
     */
    boolean reverseLikePost(Long userId, Long discussPostId);

    /**
     * 点赞评论
     */
    void likeComment(Long userId, Long commentId);

    /**
     * 取消点赞评论
     */
    void cancelLikeComment(Long userId, Long commentId);

    /**
     * 如果为已点赞，取消点赞后返回false
     * 如果为未点赞，点赞后返回true
     */
    boolean reverseLikeComment(Long userId, Long commentId);

    boolean isLikePost(Long userId, Long discussPostId);

    boolean isLikeComment(Long userId, Long commentId);
}
