package com.lysen.service.impl;

import com.lysen.common.PagingResult;
import com.lysen.common.QueryParam;
import com.lysen.model.entity.User;
import com.lysen.model.dto.FollowDto;
import com.lysen.model.follow.FolloweeType;
import com.lysen.model.follow.FolloweeTypeHelper;
import com.lysen.model.follow.UserFolloweeType;
import com.lysen.mapper.UserMapper;
import com.lysen.service.FollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import java.util.Set;

@SuppressWarnings({"all"})
/**
 * Redis的存储方式
 * 粉丝列表：follower:type(我的类型):followeeId(我的id) -> zset(followerId, time)
 * 关注列表：followee:type(被我关注的类型):followerId(我的id) -> zset(followeeId, time)
 * 基于zset，用时间time作为score排序
 */
@Service
public class FollowServiceImpl implements FollowService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private FolloweeTypeHelper  followeeTypeHelper;
    @Autowired
    private UserFolloweeType userFolloweeType;

    @Override
    public void follow(FollowDto param) {
        redisTemplate.execute(new SessionCallback() {
            @Override
            public Object execute(RedisOperations ops) throws DataAccessException {
                ops.multi();
                //关注列表
                redisTemplate.opsForZSet().add(followeeRedisKey(param), param.getFolloweeId(), System.currentTimeMillis());
                //粉丝列表
                redisTemplate.opsForZSet().add(followerRedisKey(param), param.getFollowerId(), System.currentTimeMillis());
                return ops.exec();
            }
        });
    }

    @Override
    public void cancelFollow(FollowDto param) {
        redisTemplate.execute(new SessionCallback() {
            @Override
            public Object execute(RedisOperations ops) throws DataAccessException {
                ops.multi();
                //关注列表
                redisTemplate.opsForZSet().remove(followeeRedisKey(param), param.getFolloweeId());
                //粉丝列表
                redisTemplate.opsForZSet().remove(followerRedisKey(param), param.getFollowerId());
                return ops.exec();
            }
        });
    }

    @Override
    public Long countOfFollower(FollowDto param) {
        return redisTemplate.opsForZSet().size(followerRedisKey(param));
    }

    @Override
    public Long countOfFollowee(FollowDto param) {
        return redisTemplate.opsForZSet().size(followeeRedisKey(param));
    }

    @Override
    public boolean isFollow(FollowDto param) {
        //如果不存在，则分数返回nil
        return redisTemplate.opsForZSet().score(followerRedisKey(param), param.getFollowerId()) != null;
    }

    @Override
    public PagingResult<User> followerList(QueryParam<FollowDto> queryParam) {
        PagingResult<User> result = new PagingResult<>();
        //redis的key值
        String redisKey = followerRedisKey(queryParam.getCondition());
        //获取粉丝id及其关注时间
        Set<ZSetOperations.TypedTuple> set = redisTemplate.opsForZSet().rangeWithScores(
                redisKey,
                queryParam.getSkip(),
                queryParam.getSkip() + queryParam.getSize() - 1
        );
        //粉丝一定是用户
        result.setRecords(userFolloweeType.getFolloweesByIds(set));
        result.setTotal(redisTemplate.opsForZSet().size(redisKey));
        result.setCurrent(queryParam.getCurrent());
        result.setSize(queryParam.getSize());
        result.setPages((long) Math.ceil((double) result.getTotal() / result.getSize()));
        return result;
    }

    @Override
    public PagingResult followeeList(QueryParam<FollowDto> queryParam) {
        PagingResult result = new PagingResult();
        //redis的key值
        String redisKey = followeeRedisKey(queryParam.getCondition());
        //获取关注者id及其关注时间
        Set<ZSetOperations.TypedTuple> set = redisTemplate.opsForZSet().rangeWithScores(
                redisKey,
                queryParam.getSkip(),
                queryParam.getSkip() + queryParam.getSize() - 1
        );
        //获取被关注者类型，可能是用户、帖子等等
        FolloweeType type = followeeTypeHelper.getByCode(queryParam.getCondition().getFolloweeTypeCode());
        result.setRecords(type.getFolloweesByIds(set));
        result.setTotal(redisTemplate.opsForZSet().size(redisKey));
        result.setCurrent(queryParam.getCurrent());
        result.setSize(queryParam.getSize());
        result.setPages((long) Math.ceil((double) result.getTotal() / result.getSize()));
        return result;
    }
}
