package com.lysen.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lysen.common.PagingResult;
import com.lysen.common.QueryParam;
import com.lysen.model.entity.DiscussPost;
import com.lysen.exception.BusinessException;
import com.lysen.mapper.DiscussPostMapper;
import com.lysen.repository.DiscussPostRepository;
import com.lysen.service.DiscussPostService;
import com.lysen.service.LikeService;
import com.lysen.service.UserService;
import com.lysen.util.SensitiveWordFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchPage;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"all"})
@Service
public class DiscussPostServiceImpl extends ServiceImpl<DiscussPostMapper, DiscussPost> implements DiscussPostService {
    @Autowired
    private DiscussPostMapper discussPostMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private SensitiveWordFilter filter;
    @Autowired
    private LikeService likeService;
    @Autowired
    private DiscussPostRepository discussPostRepository;

//    @Override
//    public List<DiscussPost> selectNotPaging(DiscussPost discussPost) {
//        LambdaQueryWrapper<DiscussPost> wrapper = discussPost == null ? new LambdaQueryWrapper<>() : discussPost.lambdaQueryWrapper();
//        //除去拉黑帖子、置顶排前面、最新的排前面
//        wrapper.ne(DiscussPost::getStatus, DiscussPost.Status.BLOCKED)
//                .orderByDesc(DiscussPost::getType)
//                .orderByDesc(DiscussPost::getCreateTime);
//        List<DiscussPost> result = discussPostMapper.selectList(wrapper);
//        //查询出User放入
//        result.forEach(item -> {
//            item.setUser(userService.getById(item.getUserId()));
//            item.setLikeCount(likeService.likeCountOfPost(item.getId()));
//        });
//        return result;
//    }
//
//    @Override
//    public PagingResult<DiscussPost> selectPaging(QueryParam<DiscussPost> queryParam) {
//        Page<DiscussPost> page = new Page<>(queryParam.getCurrent(), queryParam.getSize());
//        LambdaQueryWrapper<DiscussPost> wrapper =
//                queryParam.getCondition() == null ? new LambdaQueryWrapper<>() : queryParam.getCondition().lambdaQueryWrapper();
//        //除去拉黑帖子、置顶排前面、最新的排前面
//        wrapper.ne(DiscussPost::getStatus, DiscussPost.Status.BLOCKED)
//                .orderByDesc(DiscussPost::getType)
//                .orderByDesc(DiscussPost::getCreateTime);
//        List<DiscussPost> result = discussPostMapper.selectList(wrapper);
//        discussPostMapper.selectPage(page, wrapper);
//        //查询出User放入
//        page.getRecords().forEach(item -> {
//            item.setUser(userService.getById(item.getUserId()));
//            item.setLikeCount(likeService.likeCountOfPost(item.getId()));
//        });
//        return new PagingResult<>(page);
//    }

    /**
     * 新增帖子
     * @param post
     * @return
     */
    @Override
    public DiscussPost insert(DiscussPost post) {
        //过滤title字段
        if (post.getTitle() != null) {
            post.setTitle(filter(post.getTitle()));
        }
        //过滤content字段
        if (post.getContent() != null) {
            post.setContent(filter(post.getContent()));
        }
        //插入数据库
        if (discussPostMapper.insert(post) <= 0) {
            throw new BusinessException("新增帖子失败");
        } else {
            return post;
        }
    }

    /**
     * 过滤关键字，既要过滤敏感词，又要过滤HTML标签，防止黑客注入
     * @param text
     * @return
     */
    private String filter(String text) {
        //过滤敏感词
        text = filter.filter(text, "***").intern();
        //过滤HTML标签，将特殊符号（大于号、小于号）用转义字符替换
        text = HtmlUtils.htmlEscape(text).intern();
        return text;
    }

    /**
     * 评论数自增
     * @param id
     * @return
     */
    @Override
    public boolean increaseCommentCount(Long id) {
        return discussPostMapper.increaseCommentCount(id) > 0;
    }

    /**
     * 关键字（queryParam.condition）搜索，并处理高亮显式的结果
     * @param queryParam
     * @return
     */
    @Override
    public PagingResult<DiscussPost> getWithKeyWord(QueryParam<String> queryParam) {
        //分页条件，按照顶置、时间降序排序
        PageRequest pageRequest = PageRequest.of(
                queryParam.getCurrent() - 1,
                queryParam.getSize(),
                Sort.by(Sort.Direction.DESC, "type", "createTime")
        );
        //获取结果
        SearchPage<DiscussPost> page = discussPostRepository.find(
                queryParam.getCondition(),
                pageRequest
        );
        //处理高亮显示的内容
        ArrayList<DiscussPost> list = new ArrayList<>();
        for (SearchHit<DiscussPost> hit : page.getContent()) {
            //title字段
            List<String> title = hit.getHighlightField("title");
            if (title != null && !title.isEmpty()) {
                hit.getContent().setTitle(title.get(0));
            }
            //content字段
            List<String> content = hit.getHighlightField("content");
            if (content != null && !content.isEmpty()) {
                hit.getContent().setContent(content.get(0));
            }
            list.add(hit.getContent());
        }
        //查询user和likeCount
        list.forEach(item -> {
            item.setUser(userService.getById(item.getUserId()));
            item.setLikeCount(likeService.likeCountOfPost(item.getId()));
        });
        //返回结果
        PagingResult<DiscussPost> result = new PagingResult<>();
        result.setSize(queryParam.getSize());
        result.setCurrent(queryParam.getCurrent());
        result.setPages(page.getTotalPages());
        result.setTotal(page.getTotalElements());
        result.setRecords(list);
        return result;
    }

    /**
     * 非关键字查找，也从ES查找，减少数据库的压力
     * @param queryParam
     * @return
     */
    @Override
    public PagingResult<DiscussPost> getWithoutKeyWord(QueryParam<String> queryParam) {
        //分页条件，按照顶置、时间降序排序
        PageRequest pageRequest = PageRequest.of(
                queryParam.getCurrent() - 1,
                queryParam.getSize(),
                Sort.by(Sort.Direction.DESC, "type", "createTime")
        );
        //获取结果
        Page<DiscussPost> page = discussPostRepository.findAll(pageRequest);
        //查询user和likeCount
        page.getContent().forEach(item -> {
            item.setUser(userService.getById(item.getUserId()));
            item.setLikeCount(likeService.likeCountOfPost(item.getId()));
        });
        return new PagingResult<DiscussPost>(page);
    }
}
