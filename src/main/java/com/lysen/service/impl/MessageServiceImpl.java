package com.lysen.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lysen.common.PagingResult;
import com.lysen.common.QueryParam;
import com.lysen.model.entity.Message;
import com.lysen.model.vo.Conversation;
import com.lysen.mapper.MessageMapper;
import com.lysen.service.MessageService;
import com.lysen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@SuppressWarnings({"all"})
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private UserService userService;

    @Override
    public Long selectUnreadCount(Long id) {
        LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Message::getStatus, Message.Status.UNREAD)
                .isNotNull(Message::getFromId) //null是系统消息
                .eq(Message::getToId, id);     //未读数目应该是to_id是本人
        return messageMapper.selectCount(wrapper);
    }

    @Override
    public PagingResult<Conversation> selectConversationList(QueryParam<Long> queryParam) {
        PagingResult<Conversation> result = new PagingResult<>();
        //获取records
        result.setRecords(messageMapper.selectConversationList(queryParam));
        //获取other、lastestContent、selfId
        result.getRecords().forEach(conversation -> {
            String[] ids = conversation.getConversationId().split("\\_");
            conversation.setOtherId(
                    Long.parseLong(ids[0]) == queryParam.getCondition() ?
                            Long.parseLong(ids[1]): Long.parseLong(ids[0])
            );
            conversation.setOther(userService.getById(conversation.getOtherId()));
            //最新消息
            conversation.setLastestContent(
                    messageMapper.selectLastestContentOfConversation(conversation.getConversationId())
            );
            conversation.setSelfId(queryParam.getCondition());
        });
        //查询总数目total
        QueryWrapper<Message> wrapper = new QueryWrapper<>();
        wrapper.select("distinct conversation_id")
                .isNotNull("from_id") //null是系统消息
                .and(i -> i.eq("from_id", queryParam.getCondition()).or().eq("to_id", queryParam.getCondition()));
        result.setTotal(messageMapper.selectCount(wrapper));
        //设置current、size、pages
        result.setCurrent(queryParam.getCurrent());
        result.setSize(queryParam.getSize());
        result.setPages((long) Math.ceil((double)result.getTotal() / result.getSize()));
        return result;
    }

    @Override
    public Long selectUnreadCountOfSystemMessage(Long id) {
        LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Message::getStatus, Message.Status.UNREAD)
                .isNull(Message::getFromId)    //null是系统消息
                .eq(Message::getToId, id);     //未读数目应该是to_id是本人
        return messageMapper.selectCount(wrapper);
    }

    @Override
    public List<Conversation> selectConversationListOfSystemMessage(Long id) {
        //获取records
        List<Conversation> result = messageMapper.selectConversationListOfSystemMessage(id);
        //获取lastestContent
        result.forEach(conversation -> {
            //最新消息
            conversation.setLastestContent(
                    messageMapper.selectLastestContentOfSystemConversation(
                            conversation.getConversationId(),
                            id
                    )
            );
        });
        return result;
    }
}
