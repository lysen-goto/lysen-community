package com.lysen.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lysen.exception.BusinessException;
import com.lysen.model.entity.User;
import com.lysen.mapper.UserMapper;
import com.lysen.model.security.LoginUser;
import com.lysen.service.UserService;
import com.lysen.session.RedisSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

@SuppressWarnings({"all"})
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService, UserDetailsService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Value("${mine.rsession.lifetime}")
    private long lifetime;

    /**
     * 根据用户名获取LoginUser对象
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        //查询全部字段，包括密码
        wrapper.select("*");
        wrapper.eq("username", username);
        User user = userMapper.selectOne(wrapper);
        if (user == null) {
            throw new BusinessException("用户不存在");
        }
        return new LoginUser(user);
    }

    /**
     * 根据id获取用户，包含缓存
     * @param id
     * @return
     */
//    @Override
//    public User getById(Serializable id) {
//        User user = (User) redisTemplate.opsForValue().get("user:" + id);
//        //缓存没有数据
//        if (user == null) {
//            user = super.getById(id);
//            //有数据存入缓存，时长10分钟
//            if (user != null) {
//                redisTemplate.opsForValue().set("user:" + id, user, 10, TimeUnit.MINUTES);
//            }
//            return user;
//        } else {
//            return user;
//        }
//    }


}
