package com.lysen.service.impl;

import com.lysen.mapper.CommentMapper;
import com.lysen.mapper.DiscussPostMapper;
import com.lysen.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.stereotype.Service;

@SuppressWarnings({"all"})
/**
 * 评论和帖子的点赞列表用set存储
 * 用户所获得的点赞数用value存储
 */
@Service
public class LikeServiceImpl implements LikeService {
    /**
     * 帖子点赞列表前缀
     */
    private static final String redisKeyPrefixOfPost = "post:like:";
    /**
     * 评论点赞列表前缀
     */
    private static final String redisKeyPrefixOfComment = "comment:like:";
    /**
     * 用户获得点赞数
     */
    private static final String redisKeyPrefixOfUser = "user:like:";

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private DiscussPostMapper discussPostMapper;
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public Long likeCountOfPost(Long discussPostId) {
        return redisTemplate.opsForSet().size(redisKeyPrefixOfPost + discussPostId);
    }

    @Override
    public Long likeCountOfComment(Long commentId) {
        return redisTemplate.opsForSet().size(redisKeyPrefixOfComment + commentId);
    }

    @Override
    public Integer likeCountOfUser(Long userId) {
        Object result = redisTemplate.opsForValue().get(redisKeyPrefixOfUser + userId);
        return result == null ? 0 : (Integer) result;
    }

    /**
     * 基于redis事务
     */
    @Override
    public void likePost(Long userId, Long discussPostId) {
        redisTemplate.execute(new SessionCallback() {
            @Override
            public Object execute(RedisOperations ops) throws DataAccessException {
                ops.multi();
                //加入点赞列表
                ops.opsForSet().add(redisKeyPrefixOfPost + discussPostId, userId);
                //获取帖子的发布者
                Long publisherId = discussPostMapper.selectById(discussPostId).getUserId();
                //发布者获赞数加一
                ops.opsForValue().increment(redisKeyPrefixOfUser + publisherId);
                return ops.exec();
            }
        });
    }

    /**
     * 基于redis事务
     */
    @Override
    public void cancelLikePost(Long userId, Long discussPostId) {
        redisTemplate.execute(new SessionCallback() {
            @Override
            public Object execute(RedisOperations ops) throws DataAccessException {
                ops.multi();
                //从点赞列表删除
                ops.opsForSet().remove(redisKeyPrefixOfPost + discussPostId, userId);
                //获取帖子的发布者
                Long publisherId = discussPostMapper.selectById(discussPostId).getUserId();
                //发布者获赞数减一
                ops.opsForValue().decrement(redisKeyPrefixOfUser + publisherId);
                return ops.exec();
            }
        });
    }

    @Override
    public boolean reverseLikePost(Long userId, Long discussPostId) {
        //已点赞
        if (isLikePost(userId, discussPostId)) {
            cancelLikePost(userId, discussPostId);
            return false;
        }
        //未点赞
        else {
            likePost(userId, discussPostId);
            return true;
        }
    }

    @Override
    public void likeComment(Long userId, Long commentId) {
        redisTemplate.execute(new SessionCallback() {
            @Override
            public Object execute(RedisOperations ops) throws DataAccessException {
                ops.multi();
                //加入点赞列表
                ops.opsForSet().add(redisKeyPrefixOfComment + commentId, userId);
                //获取评论的发布者
                Long publisherId = commentMapper.selectById(commentId).getUserId();
                //发布者获赞数加一
                ops.opsForValue().increment(redisKeyPrefixOfUser + publisherId);
                return ops.exec();
            }
        });
    }

    @Override
    public void cancelLikeComment(Long userId, Long commentId) {
        redisTemplate.execute(new SessionCallback() {
            @Override
            public Object execute(RedisOperations ops) throws DataAccessException {
                ops.multi();
                //从点赞列表删除
                ops.opsForSet().remove(redisKeyPrefixOfComment + commentId, userId);
                //获取评论的发布者
                Long publisherId = commentMapper.selectById(commentId).getUserId();
                //发布者获赞数加一
                ops.opsForValue().decrement(redisKeyPrefixOfUser + publisherId);
                return ops.exec();
            }
        });
    }

    @Override
    public boolean reverseLikeComment(Long userId, Long commentId) {
        //已点赞
        if (isLikeComment(userId, commentId)) {
            cancelLikeComment(userId, commentId);
            return false;
        }
        //未点赞
        else {
            likeComment(userId, commentId);
            return true;
        }
    }

    @Override
    public boolean isLikePost(Long userId, Long discussPostId) {
        return redisTemplate.opsForSet().isMember(redisKeyPrefixOfPost + discussPostId, userId);
    }

    @Override
    public boolean isLikeComment(Long userId, Long commentId) {
        return redisTemplate.opsForSet().isMember(redisKeyPrefixOfComment + commentId, userId);
    }
}
