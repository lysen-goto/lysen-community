package com.lysen.service.impl;

import com.lysen.service.DataService;
import com.lysen.util.DateUtil;
import com.lysen.util.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings({"all"})
@Service
public class DataServiceImpl implements DataService {
    /**
     * UV的redis前缀
     */
    private final String REDIS_KEY_PREFIX_UV = "uv";
    /**
     * DAU的redis前缀
     */
    private final String REDIS_KEY_PREFIX_DAU = "dau";
    /**
     * 日期格式化
     */
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RedisCache redisCache;

    /**
     * 用户访问
     * @param ip
     */
    @Override
    public void addUniqueVisitor(String ip) {
        //获取当前日期
        LocalDate date = LocalDate.now();
        //日期序列化
        String dateStr = this.formatter.format(date);
        //存入redis
        redisTemplate.opsForHyperLogLog().add(
                REDIS_KEY_PREFIX_UV + ":" + dateStr,
                ip
        );
    }

    @Override
    public long countUniqueVisitor(LocalDate start, LocalDate end) {
        //获取start到end的全部日期
        List<LocalDate> dates = DateUtil.localDateList(start, end);
        //获取日期的全部key值
        String[] dateKeys = dates.stream()
                .map(date -> REDIS_KEY_PREFIX_UV + ":" + formatter.format(date))
                .collect(Collectors.toList())
                .toArray(new String[0]);
        //redis查询结果
        return redisTemplate.opsForHyperLogLog().size(dateKeys);
    }

    @Override
    public void addDailyActiveUser(Long userId) {
        //获取当前日期
        LocalDate date = LocalDate.now();
        //日期格式化
        String dateStr = formatter.format(date);
        //放入redis中
        redisTemplate.opsForValue().setBit(
                REDIS_KEY_PREFIX_DAU + ":" + dateStr,
                userId,
                true
        );
    }

    @Override
    public long countDailyActiveUser(LocalDate start, LocalDate end) {
        //日期格式化
        String startStr = formatter.format(start);
        String endStr = formatter.format(end);
        //构建合并结果的key值
        String desKey = REDIS_KEY_PREFIX_DAU + ":" + startStr + ":" + endStr;
        //构建日期数组
        String[] dateKeys = DateUtil.localDateList(start, end)
                .stream()
                .map(date -> REDIS_KEY_PREFIX_DAU + ":" + formatter.format(date))
                .collect(Collectors.toList())
                .toArray(new String[0]);
        //返回结果
        return redisCache.bigCount(desKey, dateKeys);
    }
}
