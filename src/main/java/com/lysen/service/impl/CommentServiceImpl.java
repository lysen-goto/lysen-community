package com.lysen.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lysen.common.PagingResult;
import com.lysen.common.QueryParam;
import com.lysen.model.entity.Comment;
import com.lysen.exception.BusinessException;
import com.lysen.mapper.CommentMapper;
import com.lysen.service.CommentService;
import com.lysen.service.DiscussPostService;
import com.lysen.service.LikeService;
import com.lysen.service.UserService;
import com.lysen.util.SensitiveWordFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@SuppressWarnings({"all"})
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private DiscussPostService discussPostService;
    @Autowired
    private SensitiveWordFilter filter;
    @Autowired
    private LikeService likeService;

    @Override
    public PagingResult<Comment> selectForDiscussPost(QueryParam<Long> queryParam) {
        LambdaQueryWrapper<Comment> wrapper = new LambdaQueryWrapper<>();
        //只展示未被禁用的评论，按照时间升序排列
        wrapper.eq(Comment::getEntityType, Comment.EntityType.POST)
                .eq(Comment::getEntityId, queryParam.getCondition())
                .eq(Comment::getStatus, Comment.Status.COMMON)
                .orderByAsc(Comment::getCreateTime);
        //分页
        Page<Comment> page = new Page<>(queryParam.getCurrent(), queryParam.getSize());
        commentMapper.selectPage(page, wrapper);
        //补充评论者的信息，评论评论的评论，点赞数
        page.getRecords().forEach((item) -> {
            item.setComments(selectForComment(item.getId()));
            item.setUser(userService.getById(item.getUserId()));
            //点赞数
            item.setLikeCount(likeService.likeCountOfComment(item.getId()));
        });
        return new PagingResult<>(page);
    }

    @Override
    public List<Comment> selectForComment(Long id) {
        LambdaQueryWrapper<Comment> wrapper = new LambdaQueryWrapper<>();
        //只展示未被禁用的评论，按照时间升序排列
        wrapper.eq(Comment::getEntityType, Comment.EntityType.COMMENT)
                .eq(Comment::getEntityId, id)
                .eq(Comment::getStatus, Comment.Status.COMMON)
                .orderByAsc(Comment::getCreateTime);
        //分页
        List<Comment> result = commentMapper.selectList(wrapper);
        //如果有回复对象，则查询出结果
        result.forEach((item) -> {
            if (item.getTargetId() != null) {
                item.setTarget(userService.getById(item.getTargetId()));
            }
            //评论者信息
            item.setUser(userService.getById(item.getUserId()));
            //点赞数
            item.setLikeCount(likeService.likeCountOfComment(item.getId()));
        });
        return result;
    }

    @Override
    public Comment insert(Comment comment) {
        //评论帖子需要 帖子评论数+1
        if (comment.getEntityType().equals(Comment.EntityType.POST)) {
            //事务传播类型为Required
            discussPostService.increaseCommentCount(comment.getEntityId());
        }
        //过滤敏感词
        comment.setContent(filter.filter(comment.getContent(), "***"));
        if (commentMapper.insert(comment) > 0) {
            return comment;
        } else {
            throw new BusinessException("插入失败");
        }
    }

    @Override
    public Long postIdOf(Long commentId) {
        Comment comment = commentMapper.selectById(commentId);
        while(!comment.getEntityType().equals(Comment.EntityType.POST)) {
            commentId = comment.getEntityId();
            comment = commentMapper.selectById(commentId);
        }
        return comment.getEntityId();
    }
}
