package com.lysen.service.impl;

import com.lysen.exception.BusinessException;
import com.lysen.model.entity.User;
import com.lysen.service.AuthenticationService;
import com.lysen.session.RedisSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings({"all"})
@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private RedisTemplate redisTemplate;
    @Value("${mine.rsession.lifetime}")
    private long lifetime;

    /**
     * 登录
     * @param authentication
     * @param request
     * @param response
     * @return
     */
    @Override
    public Authentication login(
            Authentication authentication,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        //调用AuthencationManager认证
        Authentication result = authenticationManager.authenticate(authentication);
        //结果为null，认证失败，抛出异常
        if (result == null) {
            throw new BusinessException("登录失败");
        }
        //认证成功，将userId放入Session
        RedisSession session = RedisSession.getRedisSession(request, response, redisTemplate, lifetime);
        session.setAttr("userId", ((User) result.getPrincipal()).getId());
        return result;
    }
}