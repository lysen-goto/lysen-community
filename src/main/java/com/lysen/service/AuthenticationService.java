package com.lysen.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings({"all"})
public interface AuthenticationService {
    /**
     * 登录
     * @param authentication
     * @return
     */
    Authentication login(
            Authentication authentication,
            HttpServletRequest request,
            HttpServletResponse response
    );

}
