package com.lysen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lysen.common.PagingResult;
import com.lysen.common.QueryParam;
import com.lysen.model.entity.DiscussPost;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@SuppressWarnings({"all"})
@Transactional
public interface DiscussPostService extends IService<DiscussPost> {
    /**
     * 新增帖子
     * @param post
     * @return
     */
    DiscussPost insert(DiscussPost post);

    /**
     * 评论数目加一
     * @param id
     * @return
     */
    boolean increaseCommentCount(Long id);

    /**
     * 分页查找，根据关键字
     * @param queryParam
     * @return
     */
    PagingResult<DiscussPost> getWithKeyWord(QueryParam<String> queryParam);

    /**
     * 分页查找，不根据关键字
     * @param queryParam
     * @return
     */
    PagingResult<DiscussPost> getWithoutKeyWord(QueryParam<String> queryParam);
}
