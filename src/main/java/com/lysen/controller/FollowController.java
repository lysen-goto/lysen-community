package com.lysen.controller;

import com.lysen.annotation.LoginRequired;
import com.lysen.annotation.PermitAll;
import com.lysen.common.QueryParam;
import com.lysen.common.Result;
import com.lysen.model.dto.FollowDto;
import com.lysen.model.follow.UserFolloweeType;
import com.lysen.event.sysmessage.ETFollowUser;
import com.lysen.event.sysmessage.Event;
import com.lysen.event.sysmessage.EventProducer;
import com.lysen.exception.ParamBlankException;
import com.lysen.service.FollowService;
import com.lysen.util.ParamUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings({"all"})
@RestController
@RequestMapping("/follow")
public class FollowController extends BaseController {
    @Autowired
    private FollowService followService;
    @Autowired
    private UserFolloweeType userFolloweeType;
    @Autowired
    private ETFollowUser etfu;
    @Autowired
    private EventProducer eventProducer;

    @PostMapping("/follow")
    public Result follow(@RequestBody FollowDto param) {
        ParamUtil.checkFieldNotBlank(
                param,
                FollowDto::getFolloweeId,
                FollowDto::getFolloweeTypeCode
        );
        param.setFollowerId(super.getUser().getId());
        followService.follow(param);
        //如果关注的是用户，发送系统消息
        if (userFolloweeType.getCode().equals(param.getFolloweeTypeCode())) {
            Event event = new Event();
            event.setEventTypeCode(etfu.getCode());
            event.setTriggerId(param.getFollowerId());
            event.setSourceId(param.getFolloweeId());
            eventProducer.fireEvent(event);
        }
        return Result.updateOk(followService.isFollow(param));
    }

    @PostMapping("/cancelFollow")
    public Result cancelFollow(@RequestBody FollowDto param) {
        ParamUtil.checkFieldNotBlank(
                param,
                FollowDto::getFolloweeId,
                FollowDto::getFolloweeTypeCode
        );
        param.setFollowerId(super.getUser().getId());
        followService.cancelFollow(param);
        return Result.updateOk(followService.isFollow(param));
    }

    @PostMapping("/isFollow")
    public Result isFollow(@RequestBody FollowDto param) {
        ParamUtil.checkFieldNotBlank(
                param,
                FollowDto::getFolloweeId,
                FollowDto::getFolloweeTypeCode
        );
        param.setFollowerId(super.getUser().getId());
        return Result.selectOk(followService.isFollow(param));
    }

    @PermitAll
    @PostMapping("/countOfFollowee")
    public Result countOfFollowee(@RequestBody FollowDto param) {
        ParamUtil.checkFieldNotBlank(
                param,
                FollowDto::getFollowerId,
                FollowDto::getFolloweeTypeCode
        );
        return Result.selectOk(followService.countOfFollowee(param));
    }

    @PermitAll
    @PostMapping("/countOfFollower")
    public Result countOfFollower(@RequestBody FollowDto param) {
        ParamUtil.checkFieldNotBlank(
                param,
                FollowDto::getFolloweeId,
                FollowDto::getFolloweeTypeCode
        );
        return Result.selectOk(followService.countOfFollower(param));
    }

    /**
     * 获取粉丝列表
     */
    @PermitAll
    @PostMapping("/followerList")
    public Result followerList(@RequestBody QueryParam<FollowDto> queryParam) {
        if (queryParam.getCondition() == null) {
            throw new ParamBlankException("condition为必传参数");
        }
        ParamUtil.checkFieldNotBlank(
                queryParam.getCondition(),
                FollowDto::getFolloweeId,
                FollowDto::getFolloweeTypeCode
        );
        return Result.selectOk(followService.followerList(queryParam));
    }

    /**
     * 获取关注列表
     */
    @PermitAll
    @PostMapping("/followeeList")
    public Result followeeList(@RequestBody QueryParam<FollowDto> queryParam) {
        if (queryParam.getCondition() == null) {
            throw new ParamBlankException("condition为必传参数");
        }
        ParamUtil.checkFieldNotBlank(
                queryParam.getCondition(),
                FollowDto::getFollowerId,
                FollowDto::getFolloweeTypeCode
        );
        return Result.selectOk(followService.followeeList(queryParam));
    }

}
