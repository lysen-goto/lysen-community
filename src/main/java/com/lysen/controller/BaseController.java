package com.lysen.controller;

import com.lysen.model.entity.User;
import com.lysen.service.UserService;
import com.lysen.session.RedisSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings({"all"})
public class BaseController {
    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected HttpServletResponse response;
    @Autowired
    protected RedisTemplate redisTemplate;
    @Value("${mine.rsession.lifetime}")
    protected Long lifetime;
    @Autowired
    protected UserService userService;

    /**
     * 获取RedisSession
     */
    protected RedisSession getRedisSession() {
        return RedisSession.getRedisSession(request, response, redisTemplate, lifetime);
    }

    /**
     * 获取登录对象
     */
    protected User getUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    /**
     * 刷新登录对象
     */
    protected User flushUser() {
        User user = userService.getById(getUser().getId());
        getRedisSession().setAttr("user", user);
        return user;
    }
}
