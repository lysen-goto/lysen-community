package com.lysen.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lysen.annotation.LoginRequired;
import com.lysen.common.PagingResult;
import com.lysen.common.QueryParam;
import com.lysen.common.Result;
import com.lysen.model.entity.Message;
import com.lysen.model.entity.User;
import com.lysen.exception.BusinessException;
import com.lysen.exception.ParamBlankException;
import com.lysen.service.MessageService;
import com.lysen.util.ParamUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@SuppressWarnings({"all"})
@RestController
@RequestMapping("/message")
public class MessageController extends BaseController {
    @Autowired
    private MessageService messageService;

    @GetMapping("/selectUnreadCount")
    public Result selectUnreadCount() {
        return Result.selectOk(messageService.selectUnreadCount(getUser().getId()));
    }

    @PostMapping("/selectConversationList")
    public Result selectConversationList(@RequestBody QueryParam<Long> queryParam) {
        queryParam.setCondition(getUser().getId());
        return Result.selectOk(messageService.selectConversationList(queryParam));
    }

    @PostMapping("/selectConversationDetail")
    public Result selectConversationDetail(@RequestBody QueryParam<String> queryParam) {
        if (queryParam.getCondition() == null) {
            throw new ParamBlankException("参数condition不能为空");
        }
        LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Message::getConversationId, queryParam.getCondition())
                .orderByAsc(Message::getCreateTime);
        Page<Message> page = messageService.page(new Page<Message>(queryParam.getCurrent(), queryParam.getSize()), wrapper);
        //判断是否是本用户的对话，不是阻止访问
        Long userId = getUser().getId();
        if (page.getRecords().isEmpty()
                || userId.equals(page.getRecords().get(0).getFromId())
                || userId.equals(page.getRecords().get(0).getToId())
        ) {
            //将消息设置为已读
            LambdaUpdateWrapper<Message> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.set(Message::getStatus, Message.Status.READ)
                    .eq(Message::getConversationId, queryParam.getCondition())
                    .eq(Message::getToId, userId);
            messageService.update(updateWrapper);
            return Result.selectOk(new PagingResult<Message>(page));
        } else {
            throw new BusinessException("无法读取其他用户的会话记录");
        }
    }

    @PostMapping
    public Result insert(@RequestBody Message message) {
        ParamUtil.checkFieldNotBlank(
                message,
                Message::getToUsername,
                Message::getContent
        );
        //获取接收者的id
        User to = userService.getOne(new LambdaQueryWrapper<User>().eq(User::getUsername, message.getToUsername()));
        if (to == null) {
            throw new BusinessException("用户不存在");
        } else {
            message.setToId(to.getId());
        }
        //默认发送者是当前用户
        message.setFromId(getUser().getId());
        message.setConversationId(
                message.getFromId() <= message.getToId() ?
                        message.getFromId() + "_" + message.getToId() :
                        message.getToId() + "_" + message.getFromId()
        );
        //设置为未读
        message.setStatus(Message.Status.UNREAD);
        if (messageService.save(message)) {
            return Result.insertOk(message);
        } else {
            throw new BusinessException("发送失败");
        }
    }

    @GetMapping("/selectUnreadCountOfSystemMessage")
    public Result selectUnreadCountOfSystemMessage() {
        return Result.selectOk(messageService.selectUnreadCountOfSystemMessage(getUser().getId()));
    }

    @GetMapping("/selectConversationListOfSystemMessage")
    public Result selectConversationListOfSystemMessage() {
        return Result.selectOk(messageService.selectConversationListOfSystemMessage(getUser().getId()));
    }

    @PostMapping("/selectSystemConversationDetail")
    public Result selectSystemConversationDetail(@RequestBody QueryParam<String> queryParam) {
        if (queryParam.getCondition() == null) {
            throw new ParamBlankException("参数condition不能为空");
        }
        //登录用户
        Long userId = getUser().getId();
        //查询结果
        LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Message::getConversationId, queryParam.getCondition())
                .eq(Message::getToId, userId)
                .orderByDesc(Message::getCreateTime);
        Page<Message> page = messageService.page(new Page<Message>(queryParam.getCurrent(), queryParam.getSize()), wrapper);
        //将消息设置为已读
        LambdaUpdateWrapper<Message> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(Message::getStatus, Message.Status.READ)
                .eq(Message::getConversationId, queryParam.getCondition())
                .eq(Message::getToId, userId);
        messageService.update(updateWrapper);
        return Result.selectOk(new PagingResult<Message>(page));
    }

}
