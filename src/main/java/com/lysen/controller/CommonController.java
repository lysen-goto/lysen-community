package com.lysen.controller;

import com.google.code.kaptcha.Producer;
import com.lysen.annotation.LoginRequired;
import com.lysen.annotation.PermitAll;
import com.lysen.common.Result;
import com.lysen.exception.BusinessException;
import com.lysen.util.FileUtil;
import com.lysen.util.MailUtil;
import com.lysen.util.QiniuUtil;
import com.lysen.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@SuppressWarnings({"all"})
@RestController
@RequestMapping("/common")
@Slf4j
public class CommonController extends BaseController {
    @Autowired
    private Producer producer;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private MailUtil mailUtil;
    @Value("${mine.verify.lifetime}")
    private Long lifetime;
    @Value("${mine.file.basePath}")
    private String fileBasePath;
    @Value("${mine.project.url}")
    private String projectUrl;
    @Autowired
    private QiniuUtil qiniuUtil;

    @PermitAll
    @GetMapping
    public void verify() throws IOException {
        //生成验证码
        String verificationCode = producer.createText();
        //放入Session中
        getRedisSession().setAttrWithLife("verificationCode", verificationCode, lifetime);
        System.out.println("验证码：" + verificationCode);
        //将图片写入response中
        ImageIO.write(
                //根据验证码生成图片
                producer.createImage(verificationCode),
                //图片格式
                "png",
                //输出流
                super.response.getOutputStream()
        );
    }

    @PermitAll
    @GetMapping("/{email}")
    public Result pwdVerify(@PathVariable("email") String email) {
        String text = producer.createText();
        //放入redis，时间是lifetime分钟
        redisTemplate.opsForValue().set(email, text, lifetime, TimeUnit.SECONDS);
        //发送邮件
        mailUtil.sendPwdVerificationMail(email, "密码重置", text);
        return Result.insertOk(true);
    }

//    /**
//     * 下载文件
//     */
//    @PermitAll
//    @GetMapping("/download/{fileName}")
//    public void download(@PathVariable("fileName") String fileName) throws IOException {
//        if (!FileUtil.download(fileBasePath + fileName, super.response.getOutputStream())) {
//            throw new BusinessException("不存在名为" + fileName + "的文件");
//        }
//    }
//
//    @PermitAll
//    @PostMapping("/upload")
//    public Result upload(@RequestParam("file") MultipartFile file) throws IOException {
//        if (file.isEmpty()){
//            throw new BusinessException("文件不能为空");
//        }
//        String url = qiniuUtil.upload(file);
//        if (url == null) {
//            throw new BusinessException("上传失败");
//        } else {
//            return Result.insertOk(url);
//        }
//    }
//
//    @PermitAll
//    @GetMapping("/delete/{key}")
//    public Result delete(@PathVariable("key") String key) throws IOException {
//        if (StringUtil.isBlank(key)){
//            throw new BusinessException("参数key不能为空");
//        }
//        if (qiniuUtil.delete(key)) {
//            return Result.deleteOk("删除成功");
//        } else {
//            throw new BusinessException("删除失败");
//        }
//    }
}
