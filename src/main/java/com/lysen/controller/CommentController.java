package com.lysen.controller;

import com.lysen.annotation.LoginRequired;
import com.lysen.annotation.PermitAll;
import com.lysen.common.QueryParam;
import com.lysen.common.Result;
import com.lysen.event.es.EsEvent;
import com.lysen.event.es.EsEventProducer;
import com.lysen.model.entity.Comment;
import com.lysen.event.sysmessage.ETCommentComment;
import com.lysen.event.sysmessage.ETCommentDiscussPost;
import com.lysen.event.sysmessage.Event;
import com.lysen.event.sysmessage.EventProducer;
import com.lysen.exception.ParamBlankException;
import com.lysen.service.CommentService;
import com.lysen.util.Context;
import com.lysen.util.ParamUtil;
import com.lysen.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@SuppressWarnings({"all"})
@RestController
@RequestMapping("/comment")
public class CommentController extends BaseController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private EventProducer eventProducer;
    @Autowired
    private ETCommentDiscussPost etcd;
    @Autowired
    private ETCommentComment etcc;
    @Autowired
    private EsEventProducer esEventProducer;

    /**
     * 返回示例————
     * {
     *     //请求状态码
     *     "code": 1,
     *     //返回数据
     *     "data": {
     *         //评论数组
     *         "records": [
     *             {
     *                 "id": "80",
     *                 "userId": "111",
     *                 //评论者信息
     *                 "user": {
     *                     "id": "111",
     *                     "username": "aaa",
     *                     "headerUrl": "http://images.nowcoder.com/head/111t.png"
     *                 },
     *                 //评论评论的评论
     *                 "comments": [
     *                     {
     *                         "id": "82",
     *                         "userId": "111",
     *                         //评论者信息
     *                         "user": {
     *                             "id": "111",
     *                             "username": "aaa"
     *                         },
     *                         "content": "嗯嗯"
     *                     },
     *                     {
     *                         "id": "83",
     *                         "userId": "111",
     *                         //评论者信息
     *                         "user": {
     *                             "id": "111",
     *                             "username": "aaa"
     *                         },
     *                         "targetId": "111",
     *                         //回复对象
     *                         "target": {
     *                             "id": "111",
     *                             "username": "aaa",
     *                         },
     *                         "content": "吃了"
     *                     }
     *                 ],
     *                 "content": "gg"
     *             },
     *             //......
     *         ],
     *         //当前页数
     *         "current": 1,
     *         //一页数目
     *         "size": 10,
     *         //总页数
     *         "pages": 1,
     *         //总评论数
     *         "total": 2
     *     },
     *     "message": null
     * }
     */
    @PermitAll
    @PostMapping("/select")
    public Result selectForDiscussPost(@RequestBody QueryParam<Long> queryParam) {
        if (StringUtil.isBlank(queryParam.getCondition())) {
            throw new ParamBlankException("参数id不能为null");
        }
        return Result.selectOk(commentService.selectForDiscussPost(queryParam));
    }

    /**
     * 评论帖子
     */
    @PostMapping("/insertForDiscussPost")
    public Result insertForDiscussPost(@RequestBody Comment comment) {
        ParamUtil.checkFieldNotBlank(
                comment,
                Comment::getEntityId,
                Comment::getContent
        );
        //默认当前用户发布
        comment.setUserId(super.getUser().getId());
        comment.setEntityType(Comment.EntityType.POST);
        //无回复对象
        comment.setTargetId(null);
        comment.setStatus(Comment.Status.COMMON);
        commentService.insert(comment);
        //发送系统通知
        Event event = new Event();
        event.setEventTypeCode(etcd.getCode());
        event.setSourceId(comment.getEntityId());
        event.setTriggerId(comment.getUserId());
        eventProducer.fireEvent(event);
        //评论后，discuss_post表的comment_count字段自增，需同步到ES中
        EsEvent esEvent = new EsEvent();
        esEvent.setType(EsEvent.Type.SAVE);
        esEvent.setDiscussPostId(comment.getEntityId());
        esEventProducer.fireEvent(esEvent);
        //重新计算帖子的热度
        redisTemplate.opsForSet().add(Context.RedisKey.POST_FLUSH_SCORE, comment.getEntityId());
        return Result.insertOk(comment);
    }

    /**
     * 评论评论
     */
    @PostMapping("/insertForComment")
    public Result insertForComment(@RequestBody Comment comment) {
        ParamUtil.checkFieldNotBlank(
                comment,
                Comment::getEntityId,
                Comment::getContent
        );
        //默认当前用户发布
        comment.setUserId(super.getUser().getId());
        comment.setEntityType(Comment.EntityType.COMMENT);
        comment.setStatus(Comment.Status.COMMON);
        commentService.insert(comment);
        //发送系统通知
        Event event = new Event();
        event.setEventTypeCode(etcc.getCode());
        event.setSourceId(comment.getTargetId() == null ? comment.getEntityId() : comment.getTargetId());
        event.setTriggerId(comment.getUserId());
        eventProducer.fireEvent(event);
        return Result.insertOk(comment);
    }

}
