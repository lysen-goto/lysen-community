package com.lysen.controller;

import com.lysen.annotation.LoginRequired;
import com.lysen.annotation.PermitAll;
import com.lysen.common.Result;
import com.lysen.event.sysmessage.ETLikeComment;
import com.lysen.event.sysmessage.ETLikeDiscussPost;
import com.lysen.event.sysmessage.Event;
import com.lysen.event.sysmessage.EventProducer;
import com.lysen.exception.ParamBlankException;
import com.lysen.service.LikeService;
import com.lysen.util.Context;
import com.lysen.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@SuppressWarnings({"all"})
@RestController
@RequestMapping("/like")
public class LikeController extends BaseController {
    @Autowired
    private LikeService likeService;
    @Autowired
    private EventProducer eventProducer;
    @Autowired
    private ETLikeComment etlc;
    @Autowired
    private ETLikeDiscussPost etld;

    @PutMapping("/reverseLikePost/{discussPostId}")
    public Result reverseLikePost(@PathVariable("discussPostId") Long discussPostId) {
        if (StringUtil.isBlank(discussPostId)) {
            throw new ParamBlankException("discussPostId不能为空");
        }
        Long userId = getUser().getId();
        boolean flag = likeService.reverseLikePost(userId, discussPostId);
        //若为点赞，发送系统通知
        if (flag) {
            Event event = new Event();
            event.setEventTypeCode(etld.getCode());
            event.setTriggerId(userId);
            event.setSourceId(discussPostId);
            eventProducer.fireEvent(event);
        }
        //重新计算帖子的热度
        redisTemplate.opsForSet().add(Context.RedisKey.POST_FLUSH_SCORE, discussPostId);
        return Result.updateOk(flag);
    }

    @PutMapping("/reverseLikeComment/{commentId}")
    public Result reverseLikeComment(@PathVariable("commentId") Long commentId) {
        if (StringUtil.isBlank(commentId)) {
            throw new ParamBlankException("commentId不能为空");
        }
        Long userId = getUser().getId();
        boolean flag = likeService.reverseLikeComment(userId, commentId);
        //若为点赞，发送系统通知
        if (flag) {
            Event event = new Event();
            event.setEventTypeCode(etlc.getCode());
            event.setTriggerId(userId);
            event.setSourceId(commentId);
            eventProducer.fireEvent(event);
        }
        return Result.updateOk(flag);
    }

    @PermitAll
    @GetMapping("/{id}")
    public Result likeCountOfUser(@PathVariable("id") Long id) {
        return Result.selectOk(likeService.likeCountOfUser(id));
    }
}
