package com.lysen.controller;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.lysen.annotation.LoginRequired;
import com.lysen.annotation.PermitAll;
import com.lysen.common.QueryParam;
import com.lysen.common.Result;
import com.lysen.event.es.EsEvent;
import com.lysen.event.es.EsEventProducer;
import com.lysen.exception.BusinessException;
import com.lysen.model.entity.DiscussPost;
import com.lysen.exception.ParamBlankException;
import com.lysen.service.DiscussPostService;
import com.lysen.service.LikeService;
import com.lysen.service.UserService;
import com.lysen.util.Context;
import com.lysen.util.DateUtil;
import com.lysen.util.ParamUtil;
import com.lysen.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@SuppressWarnings({"all"})
@RestController
@RequestMapping("/discuss_post")
public class DiscussPostController extends BaseController {
    @Autowired
    private DiscussPostService discussPostService;
    @Autowired
    private UserService userService;
    @Autowired
    private LikeService likeService;
    @Autowired
    private EsEventProducer esEventProducer;

    /**
     * 分页获取帖子列表，从ES获取，减轻MySQL的压力
     * @param queryParam
     * @return
     */
    @PermitAll
    @PostMapping("/get")
    public Result get(@RequestBody QueryParam<String> queryParam) {
        if (StringUtil.isBlank(queryParam.getCondition())) {
            //关键字查找
            return Result.selectOk(discussPostService.getWithoutKeyWord(queryParam));
        } else {
            //非关键字查找
            return Result.selectOk(discussPostService.getWithKeyWord(queryParam));
        }
    }

    /**
     * 根据id获取帖子详情
     * @param id
     * @return
     */
    @PermitAll
    @GetMapping
    public Result getById(@RequestParam("id") Long id) {
        if (StringUtil.isBlank(id)) {
            throw new ParamBlankException("id不能为空");
        }
        DiscussPost post = discussPostService.getById(id);
        //查询发布者的详细信息
        post.setUser(userService.getById(post.getUserId()));
        //设置点赞数
        post.setLikeCount(likeService.likeCountOfPost(post.getId()));
        return Result.selectOk(post);
    }

    /**
     * 新增帖子，需要登录检测
     * @param post
     * @return
     */
    @PostMapping
    public Result save(@RequestBody DiscussPost post) {
        //title和content为必传参数
        ParamUtil.checkFieldNotBlank(
                post,
                DiscussPost::getTitle,
                DiscussPost::getContent
        );
        //当前用户发布帖子
        post.setUserId(super.getUser().getId());
        //普通帖子
        post.setType(DiscussPost.Type.COMMON);
        post.setStatus(DiscussPost.Status.COMMON);
        //评论数为0
        post.setCommentCount(0l);
        post.setScore((double) DateUtil.dayDuration(LocalDate.now(), Context.communityStartTime));
        //插入到数据库中
        discussPostService.insert(post);
        //同步信息到ES中
        EsEvent event = new EsEvent();
        event.setType(EsEvent.Type.SAVE);
        event.setDiscussPostId(post.getId());
        esEventProducer.fireEvent(event);
        return Result.insertOk(post);
    }

    /**
     * 置顶
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('MODERATOR')")
    @PutMapping("/top/{id}")
    public Result top(@PathVariable("id") Long id) {
        if (id == null) {
            throw new ParamBlankException("参数id不能为空");
        }
        LambdaUpdateWrapper<DiscussPost> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(DiscussPost::getType, DiscussPost.Type.TOP);
        wrapper.eq(DiscussPost::getId, id);
        if (discussPostService.update(wrapper)) {
            //更新ES
            EsEvent event = new EsEvent();
            event.setType(EsEvent.Type.SAVE);
            event.setDiscussPostId(id);
            esEventProducer.fireEvent(event);
            return Result.updateOk(true);
        } else {
            throw new BusinessException("置顶失败");
        }
    }

    /**
     * 加精
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('MODERATOR')")
    @PutMapping("/highlight/{id}")
    public Result highlight(@PathVariable("id") Long id) {
        if (id == null) {
            throw new ParamBlankException("参数id不能为空");
        }
        LambdaUpdateWrapper<DiscussPost> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(DiscussPost::getStatus, DiscussPost.Status.HIGHLIGHT);
        wrapper.eq(DiscussPost::getId, id);
        if (discussPostService.update(wrapper)) {
            //更新ES
            EsEvent event = new EsEvent();
            event.setType(EsEvent.Type.SAVE);
            event.setDiscussPostId(id);
            esEventProducer.fireEvent(event);
            //重新计算帖子的热度
            redisTemplate.opsForSet().add(Context.RedisKey.POST_FLUSH_SCORE, id);
            return Result.updateOk(true);
        } else {
            throw new BusinessException("置顶失败");
        }
    }

    /**
     * 删帖
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable("id") Long id) {
        if (id == null) {
            throw new ParamBlankException("参数id不能为空");
        }
        if (discussPostService.removeById(id)) {
            //更新ES
            EsEvent event = new EsEvent();
            event.setType(EsEvent.Type.DELETE);
            event.setDiscussPostId(id);
            esEventProducer.fireEvent(event);
            return Result.deleteOk(true);
        } else {
            throw new BusinessException("置顶失败");
        }
    }
}
