package com.lysen.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.lysen.annotation.Anonymous;
import com.lysen.annotation.LoginRequired;
import com.lysen.annotation.PermitAll;
import com.lysen.common.Result;
import com.lysen.model.entity.User;
import com.lysen.exception.BusinessException;
import com.lysen.exception.LoginFailException;
import com.lysen.exception.ParamBlankException;
import com.lysen.model.security.NamePwdCodeAuthentication;
import com.lysen.service.AuthenticationService;
import com.lysen.service.DataService;
import com.lysen.service.UserService;
import com.lysen.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings({"all"})
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;
    @Autowired
    private DataService dataService;
    @Autowired
    private MailUtil mailUtil;
    @Autowired
    private AuthenticationService authenticationService;
    @Value("${mine.project.url}")
    private String projectUrl;
    @Value("${mine.file.basePath}")
    private String fileBasePath;
    @Autowired
    private QiniuUtil qiniuUtil;

    /**
     * 注册
     */
    @PermitAll
    @PostMapping("/register")
    public Result register(@RequestBody User user) {
        ParamUtil.checkFieldNotBlank(
                user,
                User::getUsername,
                User::getPassword,
                User::getEmail
        );
        //检测服务端验证账号是否已存在、邮箱是否已注册
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getUsername, user.getUsername())
                .or()
                .eq(User::getEmail, user.getEmail());
        if (userService.count(wrapper) > 0) {
            throw new BusinessException("账号或邮箱已注册");
        }
        //补充参数
        user.setSalt(StringUtil.uuid());
        user.setType(User.Type.COMMON);
        user.setStatus(User.Status.UNACTIVATE);
        user.setActivationCode(StringUtil.uuid());
        //密码进行MD5加密
        user.setPassword(StringUtil.md5(user.getPassword(), user.getSalt()));
        //新增
        if (!userService.save(user)) {
            throw new BusinessException("注册失败");
        }
        //服务端发送激活邮件
        mailUtil.sendActivateUrlMail(
                user.getEmail(),
                "LysenCommunity",
                //激活路径
                projectUrl + "/user/activate/" + user.getId() + "/" + user.getActivationCode()
        );
        //清除敏感信息
        user.setPassword(null);
        user.setSalt(null);
        user.setActivationCode(null);
        return Result.insertOk(user);
    }

    /**
     * 激活
     */
    @PermitAll
    @GetMapping("/activate/{id}/{activationCode}")
    public Result activate(@PathVariable("id") Long id, @PathVariable("activationCode") String activationCode) {
        if (id == null) {
            throw new ParamBlankException("参数id不能为null");
        } else if (StringUtil.isBlank(activationCode)) {
            throw new ParamBlankException("参数activationCode不能为null或空串");
        }
        LambdaUpdateWrapper<User> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(User::getId, id)
                .eq(User::getActivationCode, activationCode)
                .set(User::getStatus, User.Status.ACTIVATE);
        if (userService.update(wrapper)) {
            return Result.updateOk("激活成功");
        } else {
            throw new BusinessException("激活失败");
        }
    }

    /**
     * 登录
     */
    @Anonymous
    @PostMapping("/login")
    public Result login(
            @RequestBody User param,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        ParamUtil.checkFieldNotBlank(param, User::getUsername, User::getPassword, User::getVerificationCode);
        //待认证信息
        NamePwdCodeAuthentication authentication = new NamePwdCodeAuthentication(param, request, response);
        //登录成功
        Authentication result = authenticationService.login(authentication, request, response);
        if (result != null) {
            //置为本日活跃用户
            dataService.addDailyActiveUser(((User) result.getPrincipal()).getId());
            return Result.selectOk(true);
        }
        //登录失败
        else {
            return Result.selectOk(false);
        }
    }

    /**
     * 退出登录
     */
    @PostMapping("/logout")
    public Result logout() {
        super.getRedisSession().removeAttr("userId");
        return Result.deleteOk(true);
    }

    /**
     * 修改密码
     */
    @PutMapping("/changePwd")
    public Result updatePwd(@RequestBody User param) {
        ParamUtil.checkFieldNotBlank(
                param,
                User::getEmail,
                User::getPwdVerifyCode,
                User::getPassword
        );
        //判断验证码是否正确
        if (!param.getPwdVerifyCode().equals(redisTemplate.opsForValue().get(param.getEmail()))) {
            throw new BusinessException("验证码不正确");
        }
        //获取salt
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.select(User::getSalt)
                .eq(User::getEmail, param.getEmail());
        User user = userService.getOne(wrapper);
        if (user == null) {
            throw new BusinessException("该邮箱未注册，请先注册");
        }
        //加密密码
        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(User::getEmail, param.getEmail())
                .set(User::getPassword, StringUtil.md5(param.getPassword(), user.getSalt()));
        //修改数据库数据
        if (userService.update(updateWrapper)) {
            return Result.updateOk(true);
        } else {
            throw new BusinessException("密码修改失败");
        }
    }

    /**
     * 修改头像
     */
    @PostMapping("/changeHeader")
    public Result updateHeader(@RequestParam("file") MultipartFile file) throws IOException {
        //获取登录用户
        User user = super.getUser();
        //把之前头像删除
        if (user.getHeaderUrl() != null) {
//            FileUtil.remove(fileBasePath + StringUtil.fileNameOfUrl(user.getHeaderUrl()));
            qiniuUtil.delete(StringUtil.fileNameOfUrl(user.getHeaderUrl()));
        }
        //新头像放入本地服务器
//        String fileName = FileUtil.upload(file, fileBasePath);
        //新头像放入七牛服务器
        String newHeaderUrl = qiniuUtil.upload(file);
        //更新User表
        LambdaUpdateWrapper<User> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(User::getId, user.getId())
                .set(User::getHeaderUrl, newHeaderUrl);
//                .set(User::getHeaderUrl, projectUrl + "/common/download/" + fileName);
        userService.update(wrapper);
        flushUser();
        return Result.updateOk(true);
    }

    @GetMapping
    public Result get() {
        return Result.selectOk(super.getUser());
    }

    @PermitAll
    @GetMapping("/{id}")
    public Result getById(@PathVariable("id") Long id) {
        return Result.selectOk(userService.getById(id));
    }

    /**
     * 修改
     */
    @PutMapping
    public Result update(@RequestBody User user) {
        Long id = super.getUser().getId();
        //默认修改的是当前登录的用户
        user.setId(id);
        //禁止修改敏感信息
        user.setPassword(null);
        user.setSalt(null);
        user.setActivationCode(null);
        if (!userService.updateById(user)) {
            throw new BusinessException("修改失败");
        }
        //返回
        return Result.updateOk(flushUser());
    }

}
