package com.lysen.controller;

import com.lysen.annotation.PermitAll;
import com.lysen.common.Result;
import com.lysen.exception.BusinessException;
import com.lysen.model.dto.DurationDto;
import com.lysen.service.DataService;
import com.lysen.util.ParamUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings({"all"})
@RestController
@RequestMapping("/data")
public class DataController extends BaseController {
    @Autowired
    private DataService dataService;

    /**
     * 获取某时间段的独立访客量
     * @param durationDto
     * @return
     */
    @PermitAll
    @PostMapping("/uv/count")
    public Result uvCount(@RequestBody DurationDto durationDto) {
        ParamUtil.checkFieldNotBlank(
                durationDto,
                DurationDto::getStart,
                DurationDto::getEnd
        );
        if (durationDto.getStart().isAfter(durationDto.getEnd())) {
            throw new BusinessException("时间start必须小于end");
        }
        return Result.selectOk(dataService.countUniqueVisitor(
                durationDto.getStart(),
                durationDto.getEnd()
        ));
    }

    /**
     * 获取某时间段的日活跃用户数目
     * @param durationDto
     * @return
     */
    @PermitAll
    @PostMapping("/dau/count")
    public Result dauCount(@RequestBody DurationDto durationDto) {
        ParamUtil.checkFieldNotBlank(
                durationDto,
                DurationDto::getStart,
                DurationDto::getEnd
        );
        if (durationDto.getStart().isAfter(durationDto.getEnd())) {
            throw new BusinessException("时间start必须小于end");
        }
        return Result.selectOk(dataService.countDailyActiveUser(
                durationDto.getStart(),
                durationDto.getEnd()
        ));
    }

}
