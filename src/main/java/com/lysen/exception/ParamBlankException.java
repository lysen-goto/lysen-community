package com.lysen.exception;

@SuppressWarnings({"all"})
/**
 * 参数为null或空串异常
 */
public class ParamBlankException extends RuntimeException {
    public ParamBlankException() {
    }

    public ParamBlankException(String message) {
        super(message);
    }

    public ParamBlankException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParamBlankException(Throwable cause) {
        super(cause);
    }

    public ParamBlankException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
