package com.lysen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lysen.model.entity.User;
import org.apache.ibatis.annotations.Mapper;

@SuppressWarnings({"all"})
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
