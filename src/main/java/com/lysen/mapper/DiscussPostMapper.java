package com.lysen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lysen.model.entity.DiscussPost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@SuppressWarnings({"all"})
@Mapper
public interface DiscussPostMapper extends BaseMapper<DiscussPost> {
    @Update("update discuss_post set comment_count = comment_count + 1 where id = #{id}")
    Integer increaseCommentCount(Long id);
}
