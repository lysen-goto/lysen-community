package com.lysen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lysen.common.QueryParam;
import com.lysen.model.entity.Message;
import com.lysen.model.vo.Conversation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@SuppressWarnings({"all"})
@Mapper
public interface MessageMapper extends BaseMapper<Message> {
    /**
     * 这里的condition就是self_id
     */
    @Select("select " +
            "sum(if(status = 0 and to_id = #{condition}, 1, 0)) as unread_count, " +
            "max(create_time) as lastest_time, " +
            "count(*) as message_count, " +
            "conversation_id " +
            "from message " +
            "where from_id is not null " +
            "and (from_id = #{condition} or to_id = #{condition}) " +
            "group by conversation_id " +
            "order by lastest_time desc " +
            "limit #{skip}, #{size}")
    List<Conversation> selectConversationList(QueryParam<Long> queryParam);

    /**
     * 查询会话的最新一条消息内容
     */
    @Select("select content from message " +
            "where conversation_id = #{conversationId} " +
            "order by create_time desc " +
            "limit 0, 1")
    String selectLastestContentOfConversation(String conversationId);

    /**
     * 查询系统通知的会话列表
     */
    @Select("select " +
            "sum(if(status = 0 and to_id = #{id}, 1, 0)) as unread_count, " +
            "max(create_time) as lastest_time, " +
            "count(*) as message_count, " +
            "conversation_id " +
            "from message " +
            "where from_id is null " +
            "and to_id = #{id} " +
            "group by conversation_id " +
            "order by lastest_time desc "
    )
    List<Conversation> selectConversationListOfSystemMessage(Long id);

    /**
     * 查询系统会话的最新一条消息内容
     */
    @Select("select content from message " +
            "where conversation_id = #{conversationId} " +
            "and from_id is null " +
            "and to_id = #{toId} " +
            "order by create_time desc " +
            "limit 0, 1")
    String selectLastestContentOfSystemConversation(String conversationId, Long toId);

}
