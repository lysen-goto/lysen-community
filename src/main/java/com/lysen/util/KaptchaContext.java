package com.lysen.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Properties;

@SuppressWarnings({"all"})
@Component
@ConfigurationProperties(prefix = "mine.kaptcha")
@Data
public class KaptchaContext {
    private String width;
    private String height;
    private String size;
    private String color;
    private String string;
    private String length;
    private String noise;

    public Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("kaptcha.image.width", width);
        properties.setProperty("kaptcha.image.height", height);
        properties.setProperty("kaptcha.textproducer.font.size", size);
        properties.setProperty("kaptcha.textproducer.font.color", color);
        properties.setProperty("kaptcha.textproducer.char.string", string);
        properties.setProperty("kaptcha.textproducer.char.length", length);
        properties.setProperty("kaptcha.noise.impl", noise);
        return properties;
    }
}
