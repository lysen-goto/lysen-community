package com.lysen.util;

import com.alibaba.fastjson.JSON;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings({"all"})
public class WebUtil {
    private WebUtil() {}

    /**
     * RequestMethod和HttpMethod的等价转换
     * @param method
     * @return
     */
    public static HttpMethod convert(RequestMethod method) {
        return HttpMethod.valueOf(method.toString());
    }

    /**
     * 往response中写入Json数据
     * @param response
     * @param obj
     * @return
     */
    public static void writeJsonToResp(
            HttpServletResponse response,
            Object obj
    ) {
        try {
            response.setStatus(200);
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            response.getWriter().print(JSON.toJSONString(obj));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
