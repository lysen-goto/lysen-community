package com.lysen.util;

import java.time.LocalDate;

@SuppressWarnings({"all"})
/**
 * 全局变量/常量
 */
public class Context {
    /**
     * Redis的key值
     */
    public static class RedisKey {
        /**
         * 待刷新热度帖子id
         */
        public static final String POST_FLUSH_SCORE = "discuss_post:flush_score";
    }

    /**
     * 社区启动时间
     */
    public static final LocalDate communityStartTime
            = LocalDate.of(2023, 4, 1);

}
