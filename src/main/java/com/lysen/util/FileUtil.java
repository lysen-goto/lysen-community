package com.lysen.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings({"all"})
public class FileUtil {
    private FileUtil() {}

    /**
     * 将 file转存到目录 basePath下，并返回随机生成的文件名称 UUID
     */
    public static String upload(MultipartFile file, String basePath) throws IOException {
        //1、判断目录是否存在，不存在则创建
        File basePathFile = new File(basePath);
        if (!basePathFile.exists()) {
            basePathFile.mkdirs();
        }
        //2、为file生成一个新的文件名 (UUID)
        //(1) UUID.randomUUID()一般用于生成随机的文件名，文件后缀从file.getoriginalFilename()获取
        //(2)由于生成的UUID是随机的，且不同时刻不同机器生成的UUID是不一样的，这样增加了数据的安全性，使得图片更难被爬到
        String newFileName = UUID.randomUUID().toString() + StringUtil.suffix(file.getOriginalFilename());
        //3、将图片转存到目录basePath下，调用方法transferTo
        file.transferTo(new File(basePath + newFileName));
        //4、返回新的文件名称
        return newFileName;
    }

    /**
     * 将文件存入输出流 outputStream中
     * @param filePath 文件的全路径
     * @param outputStream 输出流
     * @return 文件存在返回 true，文件不存在返回 false
     */
    public static boolean download(String filePath, OutputStream outputStream) throws IOException {
        //1、判断文件是否存在
        if (!new File(filePath).exists()) {
            return false;
        }
        //2、读出文件，放入输出流
        FileInputStream fileInputStream = new FileInputStream(filePath);
        byte[] bytes = new byte[1024];
        int length = -1;
        while ((length = fileInputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, length);
            //一定要刷新!!!
            outputStream.flush();
        }
        //3、关闭输入流
        fileInputStream.close();
        return true;
    }

    /**
     * 删除文件
     */
    public static boolean remove(String filePath) {
        //1、判断文件是否存在
        File file = new File(filePath);
        if (!file.exists()) {
            return false;
        }
        //2、删除
        return file.delete();
    }

    /**
     * ClassLoader 默认会从 target/classes 下查找
     * src/main/java 和 resources 下的资源会直接编译到 target/classes
     * @param filePath target/classes 下的路径
     * @return 返回读取到的集合
     */
    public static List<String> readAsList(String filePath) throws IOException {
        //获取文件流
        InputStream inputStream = FileUtil.class.getClassLoader().getResourceAsStream(filePath);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<String> result = new ArrayList<>();
        //一行一个
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            result.add(line.intern());
        }
        //关闭流，返回结果
        bufferedReader.close();
        return result;
    }

}