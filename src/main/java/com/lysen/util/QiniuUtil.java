package com.lysen.util;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@SuppressWarnings({"all"})
/**
 * 七牛云工具类
 */
@Slf4j
@Component
public class QiniuUtil {
    @Value("${qiniu.bucket}")
    private String bucket;
    @Value("${qiniu.domain}")
    private String domain;
    @Value("${qiniu.ak}")
    private String ak;
    @Value("${qiniu.sk}")
    private String sk;

    /**
     * 上传图片到服务器，上传成功返回访问链接，否则返回null
     * @param file
     * @return
     * @throws IOException
     */
    public String upload(MultipartFile file) throws IOException {
        //生成新文件名
        String newFileName = UUID.randomUUID().toString() + StringUtil.suffix(file.getOriginalFilename());
        //生成身份凭证
        Auth auth = Auth.create(ak, sk);
        //生成上传凭证
        String uploadToken = auth.uploadToken(this.bucket, newFileName);
        //上传，指定区域华南
        UploadManager uploadManager = new UploadManager(new Configuration(Region.huanan()));
        try {
            //上传成功，返回访问路径
            uploadManager.put(file.getBytes(), newFileName, uploadToken);
            return "http://" + this.domain + "/" + newFileName;
        } catch (QiniuException e) {
            //上传失败
            log.error("图片上传失败，原因：" + e.response.bodyString());
            return null;
        }
    }

    /**
     * 删除图片
     * @param key
     * @return 删除成功返回true，删除失败返回false
     */
    public boolean delete(String key) {
        //生成身份凭证
        Auth auth = Auth.create(ak, sk);
        //配置
        Configuration configuration = new Configuration(Region.huanan());
        //删除
        BucketManager bucketManager = new BucketManager(auth, configuration);
        try {
            //删除成功
            bucketManager.delete(this.bucket, key);
            return true;
        } catch (QiniuException e) {
            //删除失败
            log.error("图片上传失败，原因：" + e.response.toString());
            return false;
        }
    }
}
