package com.lysen.util;

import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@SuppressWarnings({"all"})
@Component
public class RedisCache {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * JSON序列化器
     */
    private GenericFastJsonRedisSerializer serializer = new GenericFastJsonRedisSerializer();

    /**
     * 将结果按位或合并，并结算最终的结果
     * @param des
     * @param src
     * @return
     */
    public long bigCount(Object des, Object... keys) {
        //序列化
        byte[] desBytes = serializer.serialize(des);
        byte[][] keysBytes = Arrays.stream(keys)
                .map(serializer::serialize)
                .collect(Collectors.toList())
                .toArray(new byte[0][0]);
        //从redis查询结果
        return (Long) redisTemplate.execute((RedisCallback) connection -> {
            //计算结果放入des中
            connection.bitOp(RedisStringCommands.BitOperation.OR, desBytes, keysBytes);
            //查询返回结果
            return bigCount(des);
        });
    }

    /**
     * 返回BitMap中的true个数
     * @param key
     * @return
     */
    public long bigCount(Object key) {
        //序列化
        byte[] keyBytes = serializer.serialize(key);
        //返回结果
        return (Long) redisTemplate.execute((RedisCallback) connection -> {
            return connection.bitCount(keyBytes);
        });
    }
}
