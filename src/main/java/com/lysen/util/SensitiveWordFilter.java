package com.lysen.util;

import java.util.*;

@SuppressWarnings({"all"})
/**
 * 敏感词过滤器
 */
public class SensitiveWordFilter {
    /**
     * 前缀树节点
     */
    private static class Node {
        /**
         * 字符，根节点的value为null
         */
        private Character value;
        /**
         * 子节点
         */
        private Map<Character, Node> children;
        /**
         * 构造器
         */
        public Node(Character value) {
            this.value = value;
        }
        /**
         * 返回子节点，如果没有返回null
         */
        public Node getChild(Character target) {
            return children == null ? null : children.get(target);
        }
        /**
         * 返回子节点，如果没有创建
         */
        public Node getOrCreateChild(Character target) {
            if (children == null) {
                children = new HashMap<>();
            }
            return children.computeIfAbsent(target, Node::new);
        }
        /**
         * 叶子节点返回true
         */
        public boolean isLeaf() {
            return children == null || children.isEmpty();
        }
    }

    /**
     * 根节点
     */
    private Node root = new Node(null);

    /**
     * 往前缀树加入单词
     */
    private void addWord(String word) {
        Node curr = root;
        int length = word.length();
        for (int i = 0; i < length; ++i) {
            curr = curr.getOrCreateChild(word.charAt(i));
        }
    }

    /**
     * 敏感词表words不能为空，而且一旦构建了，就不能再修改
     */
    public SensitiveWordFilter(List<String> words) {
        if (words == null || words.isEmpty()) {
            throw new NullPointerException("敏感词表words不能为空");
        }
        for (String word : words) {
            addWord(word);
        }
    }

    /**
     * 将text中的敏感词替换成replacer
     */
    public String filter(String text, String replacer) {
        StringBuilder result = new StringBuilder();
        int i = 0, length = text.length();
        while (i < length) {
            Node curr = root;
            Node tmp = null;
            int j = i;
            while (j < length) {
                //到叶子节点 或者 没有text[j]的节点
                if ((tmp = curr.getChild(text.charAt(j))) == null) {
                    break;
                }
                //找到了text[j]的节点
                else {
                    curr = tmp;
                    j++;
                }
            }
            //text[i,j) 是敏感词
            if (curr.isLeaf()) {
                result.append(replacer);
                i = j;
            }
            //不是敏感词
            else {
                result.append(text.charAt(i));
                ++i;
            }
        }
        return result.toString().intern();
    }
}