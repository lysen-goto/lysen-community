package com.lysen.util;

import org.joda.time.LocalDateTime;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"all"})
/**
 * 日期工具类
 */
public class DateUtil {
    private DateUtil() {}

    /**
     * 获取[start，end]的时间数据，间隔天
     * @param start
     * @param end
     * @return
     */
    public static List<LocalDate> localDateList(LocalDate start, LocalDate end) {
        return Stream.iterate(start, date -> date.plusDays(1l))
                .limit(dayDuration(start, end) + 1)
                .collect(Collectors.toList());
    }

    /**
     * 相差天数
     * @param start
     * @param end
     * @return
     */
    public static long dayDuration(LocalDate start, LocalDate end) {
        return Duration.between(start.atStartOfDay(), end.atStartOfDay()).toDays();
    }

}
