package com.lysen.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@SuppressWarnings({"all"})
@Component
@Slf4j
public class MailUtil {
    @Autowired
    private JavaMailSender sender;
    //模板引擎，用于将html文件解析成String对象
    @Autowired
    private TemplateEngine engine;
    //发送者
    @Value("${spring.mail.username}")
    private String from;

    /**
     * 发送邮件
     * @param to 接收者
     * @param subject 主题
     * @param content 内容
     * @param html true表示html邮件，false表示普通邮件
     */
    public void sendMail(String to, String subject, String content, boolean html) {
        try {
            MimeMessageHelper helper = new MimeMessageHelper(sender.createMimeMessage());
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            //true表示html邮件，false表示普通邮件
            helper.setText(content, html);
            //发送邮件
            sender.send(helper.getMimeMessage());
        } catch (MessagingException e) {
            //日志
            log.error("邮件发送失败：" + e.getMessage());
        }
    }

    /**
     * 发送文本邮件
     * @param to 接收者
     * @param subject 主题
     * @param content 内容
     */
    public void sendTextMail(String to, String subject, String content) {
        sendMail(to, subject, content, false);
    }

    /**
     * 发送html邮件
     * @param to 接收者
     * @param subject 主题
     * @param html html文件内容
     */
    public void sendHtmlMail(String to, String subject, String html) {
        sendMail(to, subject, html, true);
    }

    /**
     * 发送激活链接邮件（基于Thymeleaf编写邮件html文件的模板）
     * @param to 接收者
     * @param subject 主题
     * @param activateUrl 激活链接
     */
    public void sendActivateUrlMail(String to, String subject, String activateUrl) {
        //context是环境变量
        Context context = new Context();
        //将传递给html的参数放入context
        context.setVariable("activateUrl", activateUrl);
        //将html文件转化String对象。指定html在 /resources/templates 下的路径，传递环境变量context
        String content = engine.process("/activateUrlMail.html", context);
        sendHtmlMail(to, subject, content);
    }

    /**
     * 重置密码发送验证码
     * @param to 接收者
     * @param subject 主题
     * @param verifyCode 验证码
     */
    public void sendPwdVerificationMail(String to, String subject, String verifyCode) {
        Context context = new Context();
        context.setVariable("verifyCode", verifyCode.toCharArray());
        String content = engine.process("/pwdVerificationMail.html", context);
        sendHtmlMail(to, subject, content);
    }

}
