package com.lysen.util;

import com.lysen.exception.ParamBlankException;
import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;

@SuppressWarnings({"all"})
public class ParamUtil {
    /**
     * 自定义函数式接口
     */
    @FunctionalInterface
    public static interface FieldFunction<T> extends Serializable {
        Object apply(T t);

        /**
         * 获取SerializedLambda对象
         */
        default SerializedLambda getSerializedLambda() {
            try {
                Method method = this.getClass().getDeclaredMethod("writeReplace");
                method.setAccessible(true);
                return (SerializedLambda) method.invoke(this);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * 获取Lambda表达式的实现方法，例如User::getId的getId
         */
        default String getImplMethodName() {
            SerializedLambda serializedLambda = getSerializedLambda();
            return serializedLambda == null ? null : serializedLambda.getImplMethodName();
        }

        /**
         * 获取Lambda表达式的字段，例如User::getId的id
         */
        default String getField() {
            String implMethodName = getImplMethodName();
            return implMethodName == null ? null : StringUtil.fieldOf(implMethodName);
        }
    }

    /**
     * 字段为null或者空串，抛出异常ParamBlankException
     */
    public static<T> void checkFieldNotBlank(T item, FieldFunction<T>... functions) {
        for (FieldFunction<T> function : functions) {
            if (StringUtil.isBlank(function.apply(item))) {
                String field = function.getField();
                throw new ParamBlankException("必传字段" + (field == null ? "" : field) + "为null或空串");
            }
        }
    }

}
