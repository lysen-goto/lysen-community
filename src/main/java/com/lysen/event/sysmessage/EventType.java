package com.lysen.event.sysmessage;

@SuppressWarnings({"all"})
/**
 * 事件类型的抽象父类
 */
public interface EventType {
    /**
     * 类型编码
     * 【1】评论帖子
     * 【2】评论评论
     * 【3】点赞帖子
     * 【4】点赞评论
     * 【5】关注用户
     */
    Integer getCode();

    /**
     * 系统消息的提示信息，如 ”用户lysen点赞了你的评论“
     */
    String getTip(Event event);

    /**
     * 获取系统消息要跳转的路径，如查看点赞你评论所在的帖子 "http://localhost:8080/site/discuss_detail?ownerId=111”
     */
    String getHref(Event event);

    /**
     * 存入Message表时的conversationId字段
     * 【评论帖子】commment
     * 【评论评论】commment
     * 【点赞帖子】like
     * 【点赞评论】like
     * 【关注用户】follow
     */
    String getConversationId(Event event);

    /**
     * 系统消息的接收者
     */
    Long getToId(Event event);
}
