package com.lysen.event.sysmessage;

import com.lysen.service.DiscussPostService;
import com.lysen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
/**
 * 事件类型：评论帖子
 * 定义成Bean，交给Spring容器管理
 */
@Component
public class ETCommentDiscussPost implements EventType {
    @Autowired
    private UserService userService;
    @Autowired
    private DiscussPostService discussPostService;
    @Value("${mine.project.url}")
    private String projectUrl;

    @Override
    public Integer getCode() {
        return Integer.valueOf(1);
    }

    @Override
    public String getTip(Event event) {
        return "用户" + userService.getById(event.getTriggerId()).getUsername() + "评论了你的帖子";
    }

    @Override
    public String getHref(Event event) {
        //这里的event.sourceId就是被评论帖子的id
        return projectUrl + "/site/discuss-detail.html?id=" + event.getSourceId();
    }

    @Override
    public String getConversationId(Event event) {
        return "comment";
    }

    @Override
    public Long getToId(Event event) {
        return discussPostService.getById(event.getSourceId()).getUserId();
    }
}
