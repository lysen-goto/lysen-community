package com.lysen.event.sysmessage;

import com.lysen.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
@Component
public class EventTypeHelper {
    @Autowired
    private ETCommentDiscussPost etcd;
    @Autowired
    private ETCommentComment etcc;
    @Autowired
    private ETLikeDiscussPost etld;
    @Autowired
    private ETLikeComment etlc;
    @Autowired
    private ETFollowUser etfu;

    public EventType getByCode(Integer code) {
        if (etcd.getCode().equals(code)) {
            return etcd;
        } else if (etcc.getCode().equals(code)) {
            return etcc;
        } else if (etld.getCode().equals(code)) {
            return etld;
        } else if (etlc.getCode().equals(code)) {
            return etlc;
        } else if (etfu.getCode().equals(code)) {
            return etfu;
        } else {
            throw new BusinessException("不存在code为" + code + "的EventType");
        }
    }
}
