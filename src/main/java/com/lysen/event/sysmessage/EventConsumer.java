package com.lysen.event.sysmessage;

import com.lysen.model.entity.Message;
import com.lysen.service.MessageService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
@Component
public class EventConsumer {
    @Autowired
    private EventTypeHelper eventTypeHelper;
    @Autowired
    private MessageService messageService;

    /**
     * 往message表中插入系统消息
     */
    @RabbitListener(queues = "system.message.queue")
    public void systemMessageQueue(Event event) {
        //获取EventType对象
        EventType eventType = eventTypeHelper.getByCode(event.getEventTypeCode());
        //构建Message对象
        Message message = new Message();
        System.out.println(eventType);
        message.setToId(eventType.getToId(event));
        message.setConversationId(eventType.getConversationId(event));
        //以JSON的格式插入
        message.setContent("{\"tip\": \"" + eventType.getTip(event) + "\", \"href\":\"" + eventType.getHref(event) + "\"}");
        message.setStatus(Message.Status.UNREAD);
        //插入数据库
        messageService.save(message);
    }
}
