package com.lysen.event.sysmessage;

import com.lysen.service.CommentService;
import com.lysen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
/**
 * 事件类型：点赞评论
 * 定义成Bean，交给Spring容器管理
 */
@Component
public class ETLikeComment implements EventType{
    @Autowired
    private UserService userService;
    @Autowired
    private CommentService commentService;
    @Value("${mine.project.url}")
    private String projectUrl;

    @Override
    public Integer getCode() {
        return Integer.valueOf(4);
    }

    @Override
    public String getTip(Event event) {
        return "用户" + userService.getById(event.getTriggerId()).getUsername() + "点赞了你的评论";
    }

    @Override
    public String getHref(Event event) {
        //这里的event.sourceId是被点赞的评论的id，获取评论所在的帖子id
        return projectUrl + "/site/discuss-detail.html?id=" + commentService.postIdOf(event.getSourceId());
    }

    @Override
    public String getConversationId(Event event) {
        return "like";
    }

    @Override
    public Long getToId(Event event) {
        return commentService.getById(event.getSourceId()).getUserId();
    }
}
