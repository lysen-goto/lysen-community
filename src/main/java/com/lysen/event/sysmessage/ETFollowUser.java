package com.lysen.event.sysmessage;

import com.lysen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
/**
 * 事件类型：关注用户
 * 定义成Bean，交给Spring容器管理
 */
@Component
public class ETFollowUser implements EventType {
    @Autowired
    private UserService userService;
    @Value("${mine.project.url}")
    private String projectUrl;

    @Override
    public Integer getCode() {
        return Integer.valueOf(5);
    }

    @Override
    public String getTip(Event event) {
        return "用户" + userService.getById(event.getTriggerId()).getUsername() + "关注了你";
    }

    @Override
    public String getHref(Event event) {
        //跳转到follower的个人主页，event.triggerId就是follower的id
        return projectUrl + "/site/profile.html?ownerId=" + event.getTriggerId();
    }

    @Override
    public String getConversationId(Event event) {
        return "follow";
    }

    @Override
    public Long getToId(Event event) {
        //event.sourceId就是被关注者的id
        return event.getSourceId();
    }
}
