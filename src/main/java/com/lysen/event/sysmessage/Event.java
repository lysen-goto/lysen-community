package com.lysen.event.sysmessage;

import lombok.Data;

import java.io.Serializable;

@SuppressWarnings({"all"})
/**
 * 事件。消息队列中，消费者和发布者的通信对象
 */
@Data
public class Event implements Serializable {
    private static final long serialVersionUID = 1l;

    /**
     * 事件触发者id，即发起 评论/点赞/关注 的人
     */
    private Long triggerId;
    /**
     * 事件源id，可以是 被评论/被点赞 的 帖子或者评论，也可以是被关注的人
     */
    private Long sourceId;
    /**
     * 事件类型编码
     * 【1】评论帖子
     * 【2】评论评论
     * 【3】点赞帖子
     * 【4】点赞评论
     * 【5】关注用户
     */
    private Integer eventTypeCode;

}
