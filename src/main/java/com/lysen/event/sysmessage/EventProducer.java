package com.lysen.event.sysmessage;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
@Component
public class EventProducer {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 触发事件
     */
    public void fireEvent(Event event) {
        rabbitTemplate.convertAndSend(
                "system.message.queue",
                event
        );
    }
}
