package com.lysen.event.es;

import com.lysen.model.entity.DiscussPost;
import lombok.Data;

@SuppressWarnings({"all"})
/**
 * ES异步同步数据库信息的事件，存放到Kafka中。新增/修改/删除帖子触发事件，服务端设置监听器消费事件
 */
@Data
public class EsEvent {
    /**
     * 类型，1表示新增和修改，2表示删除
     */
    private Integer type;

    /**
     * 变化帖子的id
     */
    private Long discussPostId;

    /**
     * 类型
     */
    public static class Type {
        /**
         * 新增和修改
         */
        public static final Integer SAVE = 1;
        /**
         * 删除
         */
        public static final Integer DELETE = 2;
    }
}
