package com.lysen.event.es;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
@Component
public class EsEventProducer {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送EsEvent到消息队列
     */
    public void fireEvent(EsEvent event) {
        rabbitTemplate.convertAndSend("es.queue", event);
    }
}
