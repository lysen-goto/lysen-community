package com.lysen.event.es;

import com.lysen.mapper.DiscussPostMapper;
import com.lysen.model.entity.DiscussPost;
import com.lysen.repository.DiscussPostRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
@Component
public class EsEventConsumer {
    @Autowired
    private DiscussPostRepository discussPostRepository;
    @Autowired
    private DiscussPostMapper discussPostMapper;

    /**
     * 监听器，刷新ES的数据
     */
    @RabbitListener(queues = "es.queue")
    public void flushEsData(EsEvent event) {
        //新增或者修改
        if (EsEvent.Type.SAVE.equals(event.getType())) {
            DiscussPost post = discussPostMapper.selectById(event.getDiscussPostId());
            //防止不存在
            if (post != null) {
                discussPostRepository.save(post);
            }
        }
        //删除
        else if (EsEvent.Type.DELETE.equals(event.getType())) {
            discussPostRepository.deleteById(event.getDiscussPostId());
        }
    }
}
