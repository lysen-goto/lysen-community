package com.lysen.session;

import com.lysen.util.StringUtil;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

@SuppressWarnings({"all"})
public class RedisSession {
    /**
     * 放入cookie时的name
     */
    public static final String cookieName = "RedisSessionId";
    /**
     * 放入request的Attribute是的name
     */
    public static final String requestAttrName = "RedisSession";
    /**
     * RedisSessionId
     */
    private final String id;
    /**
     * 放入redis时的key值，等于session:id
     */
    private final String redisKey;
    /**
     * RedisTemplate
     */
    private RedisTemplate redisTemplate;
    /**
     * session的有效时长
     */
    private long lifetime;

    /**
     * 构造器私有化，只能通过静态方法getRedisSession获取，理由如下————
     * 假设这是本次会话第一次获取RedisSession，request中没有RedisSessionId的Cookie
     * 如果在这次请求响应中，多次获取RedisSession，就会创建多个，相互覆盖
     * 因此，创建完RedisSession后，要把它放入request中，下次获取时，直接从request获取，不用重复创建
     */
    private RedisSession(HttpServletRequest request, HttpServletResponse response, RedisTemplate redisTemplate, long lifetime) {
        Cookie cookie = getByName(request, cookieName);
        //本次会话第一次获取RedisSession
        if (cookie == null) {
            cookie = new Cookie(cookieName, StringUtil.uuid());
            cookie.setPath("/");
            response.addCookie(cookie);
        }
        //放入request
        request.setAttribute(requestAttrName, this);
        this.redisTemplate = redisTemplate;
        this.id = cookie.getValue();
        this.redisKey = "session:" + id;
        this.lifetime = lifetime;
    }

    /**
     * 获取session
     */
    public static RedisSession getRedisSession(HttpServletRequest request, HttpServletResponse response, RedisTemplate redisTemplate, long lifetime) {
        RedisSession session = (RedisSession) request.getAttribute(requestAttrName);
        return session == null ? new RedisSession(request, response, redisTemplate, lifetime) : session;
    }

    /**
     * 根据name获取Cookie，不存在返回null
     */
    private Cookie getByName(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return null;
        }
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(name)) {
                return cookie;
            }
        }
        return null;
    }

    /**
     * 往Session中放入值
     */
    public void setAttr(Object key, Object value) {
        redisTemplate.opsForHash().put(redisKey, key, value);
        //一旦操作，重新设置过期时间
        redisTemplate.expire(redisKey, lifetime, TimeUnit.SECONDS);
    }

    /**
     * 从Session中获取值
     */
    public Object getAttr(Object key) {
        //一旦操作，重新设置过期时间
        redisTemplate.expire(redisKey, lifetime, TimeUnit.SECONDS);
        return redisTemplate.opsForHash().get(redisKey, key);
    }

    /**
     * 从Session中删除值
     */
    public Long removeAttr(Object key) {
        //一旦操作，重新设置过期时间
        redisTemplate.expire(redisKey, lifetime, TimeUnit.SECONDS);
        return redisTemplate.opsForHash().delete(redisKey, key);
    }

    /**
     * 往Session中放入具有生命时长的值
     */
    public void setAttrWithLife(String key, Object value, long seconds) {
        //因为hset没有过期时间，采用拼接的方式实现
        redisTemplate.opsForValue().set(redisKey + ":" + key, value, seconds, TimeUnit.SECONDS);
    }

    /**
     * 从Session中获取具有生命时长的值
     */
    public Object getAttrWithLife(String key) {
        return redisTemplate.opsForValue().get(redisKey + ":" + key);
    }
}
