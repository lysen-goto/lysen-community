package com.lysen.config;

import com.lysen.util.FileUtil;
import com.lysen.util.SensitiveWordFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@SuppressWarnings({"all"})
@Configuration
public class MineConfig {
    @Bean
    public SensitiveWordFilter sensitiveWordFilter() throws IOException {
        return new SensitiveWordFilter(FileUtil.readAsList("sensitiveWords.txt"));
    }
}
