package com.lysen.config;

import com.lysen.job.DiscussPostFlushScoreJob;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

@SuppressWarnings({"all"})
/**
 * FactoryBean和Bean的关系————
 * 1、FactoryBean能够简化Bean的初始化
 * 2、将FactoryBean注入容器后，自动装配时获取的是FactoryBean管理的Bean，而不是FactoryBean本身
 */
@Configuration
public class QuartzConfig {
    /**
     * JobDetail
     * @return
     */
    @Bean
    public JobDetailFactoryBean discussPostFlushScoreJobDetail() {
        JobDetailFactoryBean bean = new JobDetailFactoryBean();
        //任务
        bean.setJobClass(DiscussPostFlushScoreJob.class);
        //名称
        bean.setName("discussPostFlushScoreJob");
        //所属的Group。一个组、一个名称唯一确定一个任务
        bean.setGroup("discussPostFlushScoreJobGroup");
        //如果没有触发器关联任务，数据库也保留该任务的信息
        bean.setDurability(true);
        //设置任务可恢复
        bean.setRequestsRecovery(true);
        //将JobDetailFactoryBean注入容器，但是自动装配时获取的是JobDetail
        return bean;
    }

    /**
     * 常用的触发器有SimpleTrigger和CronTrigger，这里使用SimpleTrigger
     * @param discussPostFlushScoreJobDetail 参数名为@Bean的方法名，防止有多个JobDetail
     * @return
     */
    @Bean
    public SimpleTriggerFactoryBean discussPostFlushScoreTrigger(JobDetail discussPostFlushScoreJobDetail) {
        SimpleTriggerFactoryBean bean = new SimpleTriggerFactoryBean();
        //设置JobDetail
        bean.setJobDetail(discussPostFlushScoreJobDetail);
        //触发器名称
        bean.setName("discussPostFlushScoreTrigger");
        //所属的Group。一个组、一个名称唯一确定一个触发器
        bean.setGroup("discussPostFlushScoreTriggerGroup");
        //任务重复执行的时间间隔，单位ms
        bean.setRepeatInterval(20 * 1000);
        //用户存储Job状态的数据结构，默认采用JobDataMap
        bean.setJobDataMap(new JobDataMap());
        //将SimpleTriggerFactoryBean注入容器，但是自动装配时获取的是SimpleTrigger
        return bean;
    }
}
