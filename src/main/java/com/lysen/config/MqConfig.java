package com.lysen.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SuppressWarnings({"all"})
@Configuration
public class MqConfig {
    /**
     * 创建系统消息的缓冲队列
     */
    @Bean
    public Queue systemMessageQueue() {
        return new Queue("system.message.queue");
    }

    /**
     * 创建ES异步同步数据库数据的缓冲队列
     */
    @Bean
    public Queue esQueue() {
        return new Queue("es.queue");
    }

    /**
     * 采用JSON序列化MQ的消息，比JDK序列化效率更高
     */
    @Bean
    public MessageConverter jsonMessageConverter(){
        return new Jackson2JsonMessageConverter();
    }
}