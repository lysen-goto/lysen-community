package com.lysen.config;

import com.lysen.handler.web.JacksonObjectMapper;
import com.lysen.interceptor.LoginRequiredInterceptor;
import com.lysen.interceptor.UniqueVisitorInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import java.util.List;

@SuppressWarnings({"all"})
@Configuration
public class SpringMvcConfig implements WebMvcConfigurer {
//    @Autowired
//    private LoginRequiredInterceptor loginRequiredInterceptor;
    @Autowired
    private UniqueVisitorInterceptor uniqueVisitorInterceptor;

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        //新建消息转换器
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        //将映射关系对象，放入消息转换器中
        converter.setObjectMapper(new JacksonObjectMapper());
        //将消息转换器追加到SpringMVC框架的转换器集合converters中
        //追加到第一个，优先级最高，让SpringMVC优先使用我们的消息转换器
        converters.add(0, converter);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(uniqueVisitorInterceptor);
    }

}