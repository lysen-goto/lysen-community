package com.lysen.config;

import com.lysen.annotation.Anonymous;
import com.lysen.annotation.PermitAll;
import com.lysen.filter.TokenFilter;
import com.lysen.handler.security.AccessDeniedHandlerImpl;
import com.lysen.handler.security.AuthenticationEntryPointImpl;
import com.lysen.handler.security.AuthenticationProviderImpl;
import com.lysen.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.Map;

@SuppressWarnings({"all"})
/**
 * SpringSecurity的配置类
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private TokenFilter tokenFilter;
    /**
     * 请求到处理方法的映射
     * 例：请求路径为/test/chat、请求方法为GET的请求 将映射到 方法com.lysen.controller.TestController#chat()上
     */
    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;
    @Autowired
    private AuthenticationProviderImpl authenticationProvider;

//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/static/**");
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //关闭csrf
        http.csrf().disable();
        //不通过Session获取SecurityContext
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //放行静态资源
        http.authorizeRequests()
                .antMatchers(
                        "/api/**",
                        "/css/**",
                        "/img/**",
                        "/js/**",
                        "/mail/**",
                        "/index.html",
                        "/site/**",
                        "/"
                )
                .permitAll();


        /*
            请求到处理方法的映射集合
            RequestMappingInfo是请求方式和请求路径
            HandlerMethod是处理请求的controller方法
         */
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping.getHandlerMethods();
        handlerMethods.forEach((info, handlerMethod) -> {
            try {
                //controller方法包含注解PermitAll
                if (handlerMethod.getMethod().isAnnotationPresent(PermitAll.class)) {
                    restAntMatchers(http, info).permitAll();
                }
                //controller方法包含注解Anonymous
                else if (handlerMethod.getMethod().isAnnotationPresent(Anonymous.class)) {
                    restAntMatchers(http, info).anonymous();
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });


        //除有注解PermitAll和PermitAll之外，其他方法全部要求认证
        http.authorizeRequests().anyRequest().authenticated();

        //将TokenFilter加入SpringSecurity的过滤器链中，并且在UsernamePasswordAuthenticationFilter前面
        http.addFilterBefore(tokenFilter, UsernamePasswordAuthenticationFilter.class);

        //注入异常处理器
        http.exceptionHandling()
                .authenticationEntryPoint(new AuthenticationEntryPointImpl())
                .accessDeniedHandler(new AccessDeniedHandlerImpl());

        //配置自定义的AuthenticationProvider
        http.authenticationProvider(authenticationProvider);
    }

    /**
     * 设计该方法是为了提高代码的复用性
     * restful风格接口请求新增拦截条件
     * @param http
     * @param info
     * @return
     * @throws Exception
     */
    private ExpressionUrlAuthorizationConfigurer<HttpSecurity>.AuthorizedUrl restAntMatchers(
            HttpSecurity http,
            RequestMappingInfo info
    ) throws Exception {
        //请求方式，因为遵循Restful风格，所以请求方式有且只有一个
        HttpMethod httpMethod = WebUtil.convert(info.getMethodsCondition().getMethods().iterator().next());
        //请求路径，因为遵循Restful风格，所以请求路径有且只有一个
        String requestUrl = info.getActivePatternsCondition().toString(); //形如 [/user/login]
        requestUrl = requestUrl.substring(1, requestUrl.length() - 1).intern(); //形如 /user/login
//        requestUrl = requestUrl.replaceAll("\\{.+\\}", "*").intern();
        //返回结果
        return http.authorizeRequests().antMatchers(httpMethod, requestUrl);
    }

    /**
     * AuthenticationManager默认不放入Spring容器
     * AuthenticationService需要依赖于AuthenticationManager，将其暴露给Spring容器
     * @return
     * @throws Exception
     */
    @Bean
    public AuthenticationManager providerManager() throws Exception {
        return super.authenticationManagerBean();
    }
}
