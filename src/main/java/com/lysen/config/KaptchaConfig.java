package com.lysen.config;

import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import com.lysen.util.KaptchaContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SuppressWarnings({"all"})
@Configuration
public class KaptchaConfig {
    @Autowired
    private KaptchaContext kaptchaContext;

    /**
     * 【com.google.code.kaptcha.Producer】接口
     * 【com.google.code.kaptcha.impl.DefaultKaptcha】实现类
     * 【com.google.code.kaptcha.util.Config】配置类
     */
    @Bean
    public Producer producer() {
        DefaultKaptcha producer = new DefaultKaptcha();
        //Config需要传入一个Properties
        producer.setConfig(new Config(kaptchaContext.getProperties()));
        return producer;
    }
}
