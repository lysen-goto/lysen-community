package com.lysen.repository;

import com.lysen.model.entity.DiscussPost;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Highlight;
import org.springframework.data.elasticsearch.annotations.HighlightField;
import org.springframework.data.elasticsearch.annotations.HighlightParameters;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchPage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@SuppressWarnings({"all"})
/**
 * 操作ES索引discuss_post的Repository
 */
@Repository
public interface DiscussPostRepository extends ElasticsearchRepository<DiscussPost, Long> {
    /**
     * 根据关键字分页查找，关键字匹配title字段和content字段
     * @param keyWord 关键字
     * @param pageable 分页查找、排序的条件
     * @return
     */
    @Query(
            "{" +
            "    \"multi_match\": {" +
            "        \"query\": \"?0\"," +
            "        \"fields\": [\"title\", \"content\"]" +
            "    }" +
            "}"
    )
    @Highlight(
            fields = {
                    @HighlightField(name = "title"),
                    @HighlightField(name = "content")
            },
            parameters = @HighlightParameters(
                    preTags = "<em>",
                    postTags = "</em>"
            )
    )
    SearchPage<DiscussPost> find(String keyWord, Pageable pageable);
}