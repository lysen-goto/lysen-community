package com.lysen.runner;

import com.lysen.mapper.DiscussPostMapper;
import com.lysen.model.entity.DiscussPost;
import com.lysen.repository.DiscussPostRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@SuppressWarnings({"all"})
/**
 * Spring容器初始化后运行，将MySQL中的数据拷贝到ES中
 */
@Slf4j
@Component
@Order(1)
public class EsInitRunner implements ApplicationRunner {
    @Autowired
    private DiscussPostRepository discussPostRepository;
    @Autowired
    private DiscussPostMapper discussPostMapper;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //清空索引discuss_post
        discussPostRepository.deleteAll();
        //将数据库表discuss_post的数据拷贝到ES中
        List<DiscussPost> discussPosts = discussPostMapper.selectList(null);
        discussPostRepository.saveAll(discussPosts);
        //记录日志
        log.info("索引discuss_post成功载入文档，ES初始化完毕");
    }
}