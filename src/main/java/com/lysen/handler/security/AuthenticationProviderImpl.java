package com.lysen.handler.security;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lysen.common.Result;
import com.lysen.exception.BusinessException;
import com.lysen.exception.LoginFailException;
import com.lysen.model.entity.User;
import com.lysen.model.security.LoginUser;
import com.lysen.model.security.NamePwdCodeAuthentication;
import com.lysen.service.impl.UserServiceImpl;
import com.lysen.session.RedisSession;
import com.lysen.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings({"all"})
/**
 * 自定义密码比对方式
 */
@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Value("${mine.rsession.lifetime}")
    private long lifetime;

    /**
     * 认证
     * @param authentication
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        NamePwdCodeAuthentication auth = (NamePwdCodeAuthentication) authentication;
        User param = (User) auth.getPrincipal();
        HttpServletRequest request = auth.getRequest();
        HttpServletResponse response = auth.getResponse();
        //获取session
        RedisSession session = RedisSession.getRedisSession(
                request,
                response,
                redisTemplate,
                lifetime
        );

        //查询验证码是否正确
        if (!param.getVerificationCode().equals(session.getAttrWithLife("verificationCode"))) {
            throw new LoginFailException("验证码不正确");
        }

        //查询数据
        UserDetails userDetails = userService.loadUserByUsername(authentication.getName());
        if (userDetails == null) {
            throw new BusinessException("用户不存在");
        }
        User user = ((LoginUser) userDetails).getUser();

        //用户状态是否为激活
        if (user.getStatus().equals(User.Status.UNACTIVATE)) {
            throw new LoginFailException("账号尚未激活，请查看邮箱激活后再登录");
        }

        //提交的密码加密处理 md5(pwd, salt)
        //登录成功
        if (user.getPassword().equals(StringUtil.md5(
                param.getPassword(),
                user.getSalt()
        ))) {
            return new UsernamePasswordAuthenticationToken(
                    user,
                    null,
                    userDetails.getAuthorities()
            );
        }
        //登录失败抛出异常
        else {
            throw new BusinessException("密码错误");
        }
    }

    /**
     * 只支持UsernamePasswordAuthenticationToken
     * @param authentication
     * @return
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return NamePwdCodeAuthentication.class == authentication;
    }
}
