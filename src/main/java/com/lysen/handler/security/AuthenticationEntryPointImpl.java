package com.lysen.handler.security;

import com.lysen.common.Code;
import com.lysen.common.Result;
import com.lysen.util.WebUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings({"all"})
/**
 * 未认证访问需认证接口
 */
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {
    @Override
    public void commence(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authException
    ) throws IOException, ServletException {
        WebUtil.writeJsonToResp(response, new Result<>(Code.LOGOUT_ERR, null, "请先登录"));
    }
}
