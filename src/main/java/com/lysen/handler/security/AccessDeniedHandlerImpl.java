package com.lysen.handler.security;

import com.lysen.common.Code;
import com.lysen.common.Result;
import com.lysen.util.WebUtil;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings({"all"})
/**
 * 认证后访问Anonymous的接口（例如登录接口）
 * 或者认证后无访问权限
 * 就会被拦截
 */
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {
    @Override
    public void handle(
            HttpServletRequest request,
            HttpServletResponse response,
            AccessDeniedException accessDeniedException
    ) throws IOException, ServletException {
        WebUtil.writeJsonToResp(response, new Result<>(Code.NO_AUTHORITY, null, "无法访问"));
    }
}
