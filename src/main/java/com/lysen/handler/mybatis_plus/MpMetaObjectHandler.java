package com.lysen.handler.mybatis_plus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@SuppressWarnings({"all"})
@Component
public class MpMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        //createTime字段为空，则填充
        if (metaObject.getValue("createTime") == null) {
            metaObject.setValue("createTime", LocalDateTime.now());
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }
}
