package com.lysen.job;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.lysen.event.es.EsEvent;
import com.lysen.event.es.EsEventConsumer;
import com.lysen.model.entity.DiscussPost;
import com.lysen.service.DiscussPostService;
import com.lysen.service.LikeService;
import com.lysen.util.Context;
import com.lysen.util.DateUtil;
import com.mysql.cj.util.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@SuppressWarnings({"all"})
/**
 * 刷新帖子的热度
 */
@Slf4j
public class DiscussPostFlushScoreJob implements Job {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private DiscussPostService discussPostService;
    @Autowired
    private LikeService likeService;
    @Autowired
    private EsEventConsumer esEventConsumer;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //获取全部待刷新的帖子id
        List<Long> list = redisTemplate.opsForSet().pop(
                Context.RedisKey.POST_FLUSH_SCORE,
                redisTemplate.opsForSet().size(Context.RedisKey.POST_FLUSH_SCORE)
        );
        //遍历
        list.forEach(id -> {
            //计算分数
            //score = log(精华分 + 评论数 * 10 + 点赞数 * 2) + (发布时间 - 社区启动时间)
            DiscussPost post = discussPostService.getById(id);
            double w = (post.getStatus().equals(DiscussPost.Status.HIGHLIGHT) ? 75 : 0)
                    + post.getCommentCount() * 10
                    + likeService.likeCountOfPost(id);
            double score = Math.log10(w > 1 ? w : 1) + DateUtil.dayDuration(Context.communityStartTime, LocalDate.now());
            //更新到数据库
            LambdaUpdateWrapper<DiscussPost> wrapper = new LambdaUpdateWrapper<>();
            wrapper.set(DiscussPost::getScore, score);
            wrapper.eq(DiscussPost::getId, id);
            discussPostService.update(wrapper);
            //通知ES更新数据
            EsEvent event = new EsEvent();
            event.setType(EsEvent.Type.SAVE);
            event.setDiscussPostId(id);
            esEventConsumer.flushEsData(event);
        });
        log.info("帖子热度更新完毕");
    }
}
