package com.lysen.common;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;
import java.util.List;

@SuppressWarnings({"all"})
@Getter
@Setter
public class PagingResult<T> {
    private List<T> records;
    private long current;
    private long size;
    private long pages;
    private long total;

    public PagingResult() {}

    public PagingResult(IPage<T> page) {
        records = page.getRecords();
        current = page.getCurrent();
        size = page.getSize();
        pages = page.getPages();
        total = page.getTotal();
    }

    public PagingResult(Page<T> page) {
        this.current = page.getNumber();
        this.size = page.getSize();
        this.pages = page.getTotalPages();
        this.total = page.getTotalElements();
        this.records = page.getContent();
    }
}