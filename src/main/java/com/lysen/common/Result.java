package com.lysen.common;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"all"})
/**
 * 不提供失败的static方法，因为有异常处理器拦截
 */
@Getter
@Setter
public class Result<T> {
    private Integer code;
    private T data;
    private String message;

    public Result() {
    }

    public Result(Integer code, T data) {
        this.code = code;
        this.data = data;
    }

    public Result(Integer code, T data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public static<E> Result insertOk(E data) {
        return new Result(Code.INSERT_OK, data);
    }

    public static<E> Result deleteOk(E data) {
        return new Result(Code.DELETE_OK, data);
    }

    public static<E> Result updateOk(E data) {
        return new Result(Code.UPDATE_OK, data);
    }

    public static<E> Result selectOk(E data) {
        return new Result(Code.SELECT_OK, data);
    }
}