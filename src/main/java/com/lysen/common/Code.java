package com.lysen.common;

@SuppressWarnings({"all"})
public class Code {
    private Code() {}

    public static final Integer INSERT_OK = 1;
    public static final Integer DELETE_OK = 1;
    public static final Integer UPDATE_OK = 1;
    public static final Integer SELECT_OK = 1;

    public static final Integer UNKNOWN_ERR = 501;
    public static final Integer PARAM_BLANK_ERR = 502;
    public static final Integer BUSINESS_ERR = 503;
    public static final Integer SQL_ERR = 504;

    public static final Integer LOGIN_OK = 1;
    public static final Integer LOGIN_FAIL_ERR = 505;
    public static final Integer LOGOUT_ERR = 506; //未登录异常
    public static final Integer NO_AUTHORITY = 507; //权限不够异常
}