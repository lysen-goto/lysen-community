package com.lysen.common;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({"all"})
/**
 * 查询传入的参数
 */
@Getter
@Setter
public class QueryParam<T> {
    //传入的查询条件，封装为POJO
    private T condition;
    //是否分页，默认不分页
    private Boolean paging = true;
    //第几页，默认第一页
    private Integer current = 1;
    //每页几条，默认一页10条
    private Integer size = 10;

    public void setCurrentIfNull(Integer current) {
        if (this.current == null)
            this.current = current;
    }

    public void setSizeIfNull(Integer size) {
        if (this.size == null)
            this.size = size;
    }

    /**
     * 跳过几条数据
     */
    public Integer getSkip() {
        return size * (current - 1);
    }
}