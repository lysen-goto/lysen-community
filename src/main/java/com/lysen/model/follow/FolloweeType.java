package com.lysen.model.follow;

import org.springframework.data.redis.core.ZSetOperations;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SuppressWarnings({"all"})
/**
 * 被关注者类型：用户、帖子、题目……
 */
public interface FolloweeType {
    /**
     * 类型编号
     */
    Integer getCode();

    /**
     * 根据followeeId获取实体
     */
    Object getFolloweeById(ZSetOperations.TypedTuple tuple);

    /**
     * 根据followeeIds获取实体
     */
    default List getFolloweesByIds(Set<ZSetOperations.TypedTuple> set) {
        ArrayList result = new ArrayList();
        for (ZSetOperations.TypedTuple tuple : set) {
            result.add(getFolloweeById(tuple));
        }
        return result;
    }
}
