package com.lysen.model.follow;

import com.lysen.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
/**
 * FolloweeType的使用帮助类，定义成Bean，方便获取FolloweeType的子类
 */
@Component
public class FolloweeTypeHelper {
    @Autowired
    private UserFolloweeType userFolloweeType;
    @Autowired
    private DiscussPostFolloweeType discussPostFolloweeType;

    /**
     * 根据编码获取FolloweeType实体，不存在返回null
     */
    public FolloweeType getByCode(Integer code) {
        if (userFolloweeType.getCode().equals(code)) {
            return userFolloweeType;
        } else if (discussPostFolloweeType.getCode().equals(code)) {
            return discussPostFolloweeType;
        } else {
            throw new BusinessException("不存在编码为" + code + "的实体类型");
        }
    }
}
