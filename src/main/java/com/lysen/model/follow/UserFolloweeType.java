package com.lysen.model.follow;

import com.lysen.model.entity.User;
import com.lysen.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@SuppressWarnings({"all"})
/**
 * 用户类型，定义为Bean，才可以获取UserMapper
 */
@Component
public class UserFolloweeType implements FolloweeType {
    @Autowired
    private UserMapper userMapper;

    @Override
    public Integer getCode() {
        return Integer.valueOf(1);
    }

    @Override
    public Object getFolloweeById(ZSetOperations.TypedTuple tuple) {
        User user = userMapper.selectById((Long) tuple.getValue());
        user.setFollowTime(
                LocalDateTime.ofEpochSecond(tuple.getScore().longValue()/1000, 0, ZoneOffset.ofHours(8))
        );
        return user;
    }

}
