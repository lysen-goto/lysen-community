package com.lysen.model.follow;

import com.lysen.mapper.DiscussPostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

@SuppressWarnings({"all"})
/**
 * 帖子类型，定义为Bean，才可以获取DiscussPostMapper
 */
@Component
public class DiscussPostFolloweeType  implements FolloweeType {
    @Autowired
    private DiscussPostMapper discussPostMapper;

    @Override
    public Integer getCode() {
        return Integer.valueOf(2);
    }

    @Override
    public Object getFolloweeById(ZSetOperations.TypedTuple tuple) {
        return null;
    }

}