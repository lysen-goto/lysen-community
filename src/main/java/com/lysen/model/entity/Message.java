package com.lysen.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@SuppressWarnings({"all"})
@Data
public class Message implements Serializable {
    public static final long serialVersionUID = 1l;

    private Long id;
    private Long fromId;
    private Long toId;
    @TableField(exist = false)
    private String toUsername;
    private String conversationId;
    private String content;
    private Integer status;
    private LocalDateTime createTime;

    public static class Status {
        //未读
        public static final Integer UNREAD = 0;
        //已读
        public static final Integer READ = 1;
        //删除
        public static final Integer DELETED = 2;
    }
}
