package com.lysen.model.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.Serializable;
import java.time.LocalDateTime;

@SuppressWarnings({"all"})
@Data
public class User implements Serializable {
    public static final long serialVersionUID = 1l;

    @TableId(type = IdType.AUTO)
    private Long id;
    private String username;
    @TableField(select = false)
    private String password;
    @TableField(select = false)
    private String salt;
    private String email;
    private Integer type;
    private Integer status;
    @TableField(select = false)
    private String activationCode;
    private String headerUrl;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    //登录验证码
    @TableField(exist = false)
    private String verificationCode;
    //重置密码验证码
    @TableField(exist = false)
    private String pwdVerifyCode;
    //关注时间
    @TableField(exist = false)
    private LocalDateTime followTime;

    public static class Type {
        public static final Integer COMMON = 0;
        public static final Integer SUPER_ADMIN = 1;
        public static final Integer VERSION_HOST = 2;
    }

    public static class Status {
        public static final Integer ACTIVATE = 1;
        public static final Integer UNACTIVATE = 0;
    }

    /**
     * 用户类型status到Authority的枚举类型
     */
    public static enum AuthorityType {
        //普通用户
        USER(0, new SimpleGrantedAuthority("USER")),
        //管理员
        ADMIN(1, new SimpleGrantedAuthority("ADMIN")),
        //版主
        MODERATOR(2, new SimpleGrantedAuthority("MODERATOR"));

        private int type;
        private GrantedAuthority authority;

        private AuthorityType(int type, GrantedAuthority authority) {
            this.type = type;
            this.authority = authority;
        }

        /**
         * 根据status获取AuthorityType对象
         * @param status
         * @return
         */
        public static AuthorityType getByType(int type) {
            AuthorityType[] types = AuthorityType.values();
            for (AuthorityType authorityType : types) {
                if (authorityType.type == type) {
                    return authorityType;
                }
            }
            throw new RuntimeException("不存在type=" + type + "的AuthorityType");
        }

        public int getType() {
            return type;
        }

        public GrantedAuthority getAuthority() {
            return authority;
        }
    }

    public static String redisKey(Long id) {
        return "like:user:" + id;
    }
}
