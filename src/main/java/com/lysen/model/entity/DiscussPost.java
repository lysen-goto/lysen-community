package com.lysen.model.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.time.LocalDateTime;

@SuppressWarnings({"all"})
@Data
/**
 * 与ES的索引的对应关系
 * indexName：索引名
 * shards：分片数目
 * replicas：副本数目
 */
@Document(indexName = "discuss_post")
public class DiscussPost implements Serializable {
    public static final long serialVersionUID = 1l;

    /**
     * 主键id
     */
    @TableId
    @Id
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户的user对象，根据uesrId查询，不进行存储
     */
    @TableField(exist = false)
    @Field(store = false)
    private User user;

    /**
     * 标题
     * analyzer是存储时的分词器，选择能拆分出更多词的分词器，最大化其被匹配到的可能性
     * searchAnalyzer是搜索时的分词器，选择拆分出较少词的分词器，提高搜索的效率和精准度
     */
    @Field(type = FieldType.Text, analyzer = "ik_max_word", searchAnalyzer = "ik_smart")
    private String title;

    /**
     * 内容
     * analyzer是存储时的分词器，选择能拆分出更多词的分词器，最大化其被匹配到的可能性
     * searchAnalyzer是搜索时的分词器，选择拆分出较少词的分词器，提高搜索的效率和精准度
     */
    @Field(type = FieldType.Text, analyzer = "ik_max_word", searchAnalyzer = "ik_smart")
    private String content;

    /**
     * 类型
     * 0表示普通，1表示置顶
     */
    private Integer type;

    /**
     * 状态
     * 0表示正常，1表示精华，2表示删除
     */
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT) //自动填充
    private LocalDateTime createTime;

    /**
     * 评论数目
     */
    private Long commentCount;

    /**
     * 分数
     */
    private Double score;

    /**
     * 点赞数目
     * 不进行存储
     */
    @TableField(exist = false)
    @Field(store = false)
    private Long likeCount;

    /**
     * 逻辑删除
     */
    @TableLogic(value = "0", delval = "1")
    private Integer deleted;
    /**
     * 获取查询的wrapper
     * @return
     */
    public LambdaQueryWrapper<DiscussPost> lambdaQueryWrapper() {
        LambdaQueryWrapper<DiscussPost> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(id != null, DiscussPost::getId, id)
                .eq(userId != null, DiscussPost::getUserId, userId);
        return wrapper;
    }

    /**
     * 类型
     */
    public static class Type {
        /**
         * 正常
         */
        public static final Integer COMMON = 0;
        /**
         * 置顶
         */
        public static final Integer TOP = 1;
    }

    /**
     * 状态
     */
    public static class Status {
        /**
         * 正常
         */
        public static final Integer COMMON = 0;
        /**
         * 精华
         */
        public static final Integer HIGHLIGHT = 1;
        /**
         * 删除，禁用
         */
        public static final Integer BLOCKED = 2;
    }
}
