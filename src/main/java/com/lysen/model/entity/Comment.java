package com.lysen.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@SuppressWarnings({"all"})
@Data
public class Comment {
    public static final long serialVersionUID = 1l;

    @TableId
    private Long id;
    /**
     * 评论者id
     */
    private Long userId;
    /**
     * 评论者对象
     */
    @TableField(exist = false)
    private User user;
    /**
     * 评论帖子的为1
     * 评论评论的为2
     */
    private Integer entityType;
    public static class EntityType {
        public static final Integer POST = 1;
        public static final Integer COMMENT = 2;
    }
    /**
     * 当entityType=2时，可能有评论该评论的评论（有点绕）
     */
    @TableField(exist = false)
    private List<Comment> comments;
    /**
     * 被评论的 帖子或评论 的id
     */
    private Long entityId;
    /**
     * 评论中回复对象的id
     */
    private Long targetId;
    /**
     * 评论中回复对象
     */
    @TableField(exist = false)
    private User target;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 帖子状态
     */
    private Integer status;
    public static class Status {
        public static final Integer COMMON = 0;
        public static final Integer FORBIDDEN = 0;
    }
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 点赞数目
     */
    @TableField(exist = false)
    private Long likeCount;
}
