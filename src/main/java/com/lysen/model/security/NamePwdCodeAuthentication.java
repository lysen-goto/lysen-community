package com.lysen.model.security;

import com.lysen.model.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@SuppressWarnings({"all"})
/**
 * 待认证信息
 * 包括账号、密码、验证码、request、response
 */
public class NamePwdCodeAuthentication implements Authentication {
    /**
     * 账号、密码、验证码
     */
    private User param;
    /**
     * 请求
     */
    private HttpServletRequest request;
    /**
     * 响应
     */
    private HttpServletResponse response;

    public NamePwdCodeAuthentication(
            User param,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        this.param = param;
        this.request = request;
        this.response = response;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return this.param;
    }

    /**
     * 本类只用于待认证，因此authenticated设置为false
     * @return
     */
    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    }

    @Override
    public String getName() {
        return param.getUsername();
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }
}
