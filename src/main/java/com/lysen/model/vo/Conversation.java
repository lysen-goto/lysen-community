package com.lysen.model.vo;

import com.lysen.model.entity.User;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@SuppressWarnings({"all"})
/**
 * 一个对话
 */
@Data
public class Conversation implements Serializable  {
    public static final long serialVersionUID = 1l;
    /**
     * 对话id
     */
    private String conversationId;
    /**
     * 本人id
     */
    private Long selfId;
    /**
     * 对方id
     */
    private Long otherId;
    /**
     * 对方User对象
     */
    private User other;
    /**
     * 未读消息数
     */
    private Long unreadCount;
    /**
     * 最新消息时间
     */
    private LocalDateTime lastestTime;
    /**
     * 消息数目
     */
    private Long messageCount;
    /**
     * 最新消息
     */
    private String lastestContent;
}
