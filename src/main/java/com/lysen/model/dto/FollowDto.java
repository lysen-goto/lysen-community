package com.lysen.model.dto;

import lombok.Data;

@SuppressWarnings({"all"})
/**
 * 请求参数
 */
@Data
public class FollowDto {
    /**
     * 关注者id
     */
    private Long followerId;

    /**
     * 被关注者类型的编号
     */
    private Integer followeeTypeCode;

    /**
     * 被关注者id
     */
    private Long followeeId;

}
