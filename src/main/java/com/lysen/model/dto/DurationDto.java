package com.lysen.model.dto;

import lombok.Data;

import java.time.LocalDate;

@SuppressWarnings({"all"})
@Data
public class DurationDto {
    /**
     * 起始时间
     */
    private LocalDate start;
    /**
     * 结束时间
     */
    private LocalDate end;
}
