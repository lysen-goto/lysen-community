package com.lysen.advice;

import com.lysen.common.Code;
import com.lysen.common.Result;
import com.lysen.exception.BusinessException;
import com.lysen.exception.LoginFailException;
import com.lysen.exception.LogoutException;
import com.lysen.exception.ParamBlankException;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@SuppressWarnings({"all"})
@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(ParamBlankException.class)
    public Result doParamBlankException(ParamBlankException exception) {
        return new Result(Code.PARAM_BLANK_ERR, null, exception.getMessage());
    }

    @ExceptionHandler(BusinessException.class)
    public Result doBusinessException(BusinessException exception) {
        return new Result(Code.BUSINESS_ERR, null, exception.getMessage());
    }

    @ExceptionHandler(LoginFailException.class)
    public Result doLoginFailException(LoginFailException exception) {
        return new Result(Code.LOGIN_FAIL_ERR, null, exception.getMessage());
    }

    @ExceptionHandler(LogoutException.class)
    public Result doLogoutException(LogoutException e) {
        return new Result(Code.LOGOUT_ERR, null, "请先登录");
    }

    @ExceptionHandler(FileSizeLimitExceededException.class)
    public Result doFileSizeLimitExceededException(FileSizeLimitExceededException e) {
        return new Result(Code.BUSINESS_ERR, null, "图片大小不能超过 1048576 bytes");
    }
}
