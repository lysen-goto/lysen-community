package com.lysen.filter;

import com.lysen.model.entity.User;
import com.lysen.model.security.LoginUser;
import com.lysen.service.UserService;
import com.lysen.session.RedisSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings({"all"})
/**
 * token过滤器，放在UsernamePasswordAuthenticationFilter前面
 * 如果已登录，创建Authentication对象放入SecurityContextHolder.getContext()中
 */
@Component
public class TokenFilter extends OncePerRequestFilter {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserService userService;
    @Value("${mine.rsession.lifetime}")
    protected Long lifetime;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException {
        //从Session中获取登录信息
        RedisSession session = RedisSession.getRedisSession(request, response, redisTemplate, lifetime);
        //查询登录用户id
        Long userId = (Long) session.getAttr("userId");
        //如果登录了，创建Authentication对象，放入SecurityContextHolder.getContext()中
        if (userId != null) {
            User user = userService.getById(userId);
            //使用三个参数的构造器，该构造器会调用setAuthenticated(true)
            //放入登录用户对象，以及权限
            UsernamePasswordAuthenticationToken authentication =
                    new UsernamePasswordAuthenticationToken(user, null, new LoginUser(user).getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        //放行
        filterChain.doFilter(request, response);
    }
}