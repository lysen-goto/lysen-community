
function reverse_like_post(discussPostId) {
    return axios.put("/like/reverseLikePost/" + discussPostId, {})
}

function reverse_like_comment(commentId) {
    return axios.put("/like/reverseLikeComment/" + commentId, {})
}

function like_count_of_user(id) {
    return axios.get("/like/" + id)
}
