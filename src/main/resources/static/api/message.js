
function message_selectUnreadCount() {
    return axios.get("/message/selectUnreadCount")
}

function message_selectConversationList(queryParam) {
    return axios.post("/message/selectConversationList", queryParam)
}

function message_selectConversationDetail(queryParam) {
    return axios.post("/message/selectConversationDetail", queryParam)
}

function message_insert(message) {
    return axios.post("/message", message)
}