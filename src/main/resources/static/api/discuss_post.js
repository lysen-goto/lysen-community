
function discuss_post_select(queryParam) {
    return axios.post("/discuss_post/get", queryParam)
}

function discuss_post_insert(discuss_post) {
    return axios.post("/discuss_post", discuss_post)
}

function discuss_post_get(id) {
    return axios.get("/discuss_post?id=" + id)
}