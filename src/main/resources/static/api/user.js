
function user_register(user) {
    return axios.post("/user/register", user);
}

function user_login(user) {
    return axios.post("/user/login", user);
}

function user_logout() {
    return axios.get("/user/logout");
}

function user_changePwd(param) {
    return axios.put("/user/changePwd", param)
}

function user_get() {
    return axios.get("/user")
}

function user_getById(id) {
    return axios.get("/user/" + id)
}

function user_update(user) {
    return axios.put("/user", user)
}