
function common_verifyUrl() {
    return "/common?time=" + (new Date()).getTime()
}

function common_pwdVerify(email) {
    return axios.get("/common/" + email)
}

function common_upload(file) {
    return axios.post("/common/upload", {
        file: file
    })
}