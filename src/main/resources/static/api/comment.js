
function comment_select(queryParam) {
    return axios.post("/comment/select", queryParam)
}

function comment_insertForDiscussPost(comment) {
    return axios.post("/comment/insertForDiscussPost", comment)
}

function comment_insertForComment(comment) {
    return axios.post("/comment/insertForComment", comment)
}