package com.lysen.util;

import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Random;

@SuppressWarnings({"all"})
@SpringBootTest
public class RedisTests {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 场景1：插入重复的数据，统计其独立总数
     */
    @Test
    void hyperLogLogTest1() {
        String key = "test:hyperLogLog";
        Random random = new Random();
        //插入数据
        for (int i = 0; i < 10000; ++i) {
            redisTemplate.opsForHyperLogLog().add(key, i);
        }
        //插入重复数据
        for (int i = 0; i < 10000; ++i) {
            redisTemplate.opsForHyperLogLog().add(key, random.nextInt(10000));
        }
        //统计数据，结果是9987，误差较小
        System.out.println(redisTemplate.opsForHyperLogLog().size(key));
    }

    /**
     * 场景2：统计多个key值的独立总数，不同key值之间的相同value也要合并
     */
    @Test
    void hyperLogLogTest2() {
        String key1 = "test:hyperLogLog1";
        String key2 = "test:hyperLogLog2";
        String key3 = "test:hyperLogLog3";
        //插入数据
        for (int i = 0; i < 100; ++i) {
            redisTemplate.opsForHyperLogLog().add(key1, i);
        }
        for (int i = 0; i < 200; ++i) {
            redisTemplate.opsForHyperLogLog().add(key2, i);
        }
        for (int i = 0; i < 300; ++i) {
            redisTemplate.opsForHyperLogLog().add(key3, i);
        }
        //统计三个key的独立总数，结果是302，误差较小
        System.out.println(redisTemplate.opsForHyperLogLog().size(key1, key2, key3));
    }

    /**
     * 场景3：记录某系统的一天用户的签到记录
     */
    @Test
    void bigMapTest1() {
        String key = "test:bigMap";
        //模拟在4号用户、8号用户、10号用户签到了
        redisTemplate.opsForValue().setBit(key, 4, true);
        redisTemplate.opsForValue().setBit(key, 8, true);
        redisTemplate.opsForValue().setBit(key, 10, true);
        //查询
        System.out.println(redisTemplate.opsForValue().getBit(key, 1)); //false
        System.out.println(redisTemplate.opsForValue().getBit(key, 4)); //true
        System.out.println(redisTemplate.opsForValue().getBit(key, 8)); //true
        //统计一天签到用户的总数
        GenericFastJsonRedisSerializer serializer = new GenericFastJsonRedisSerializer ();
        Long count = (Long) redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                //项目配置Redis的序列化器是GenericFastJsonRedisSerializer
                //调用底层connection需手动序列化
                return connection.bitCount(serializer.serialize(key));
            }
        });
        System.out.println(count); //3
    }

    /**
     * 场景4：记录系统三天用户签到的总数（同一个用户只算一次）
     */
    @Test
    void bigMapTest2() {
        String key1 = "test:bigMap1";
        String key2 = "test:bigMap2";
        String key3 = "test:bigMap3";
        //模拟签到
        //第一天4、5签到，第二天8、9签到，第三天9、11签到
        redisTemplate.opsForValue().setBit(key1, 4, true);
        redisTemplate.opsForValue().setBit(key1, 5, true);
        redisTemplate.opsForValue().setBit(key2, 8, true);
        redisTemplate.opsForValue().setBit(key2, 9, true);
        redisTemplate.opsForValue().setBit(key3, 9, true);
        redisTemplate.opsForValue().setBit(key3, 11, true);
        //统计该用户三月总的签到数目
        GenericFastJsonRedisSerializer serializer = new GenericFastJsonRedisSerializer ();
        Long count = (Long) redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                //合并放入test::bigMapCount
                String resultKey = "test::bigMapCount";
                //按位或
                Long res = connection.bitOp(
                        RedisStringCommands.BitOperation.OR,
                        serializer.serialize(resultKey),
                        serializer.serialize(key1),
                        serializer.serialize(key2),
                        serializer.serialize(key3)
                );
                System.out.println("res = " + res);
                //取出结果
                return connection.bitCount(serializer.serialize(resultKey));
            }
        });
        System.out.println(count); //5
    }

    @Test
    void test() {

    }
}
