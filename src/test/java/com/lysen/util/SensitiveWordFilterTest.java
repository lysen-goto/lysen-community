package com.lysen.util;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings({"all"})
@SpringBootTest
public class SensitiveWordFilterTest {
    @Test
    void test() {
        List<String> words = Arrays.asList(
                "你妈",
                "嫖娼",
                "cnm",
                "草泥马",
                "屌你",
                "你他妈的",
                "傻逼",
                "傻狗子"
        );
        SensitiveWordFilter filter = new SensitiveWordFilter(words);
        String s = filter.filter("cncnm我是你爹，你知道吗？你如果不知道，你就是傻逼了好吧！你他妈的说句话啊啊啊啊", "***");
        System.out.println(s);
    }
}
