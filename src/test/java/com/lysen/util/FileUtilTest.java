package com.lysen.util;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SuppressWarnings({"all"})
@SpringBootTest
public class FileUtilTest {
    @Test
    void test() throws IOException {
        System.out.println(FileUtil.readAsList("sensitiveWords.txt"));
    }
}
