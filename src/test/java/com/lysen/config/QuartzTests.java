package com.lysen.config;

import org.junit.jupiter.api.Test;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SuppressWarnings({"all"})
@SpringBootTest
public class QuartzTests {
    //调度器
    @Autowired
    private Scheduler scheduler;

    @Test
    void delete() throws SchedulerException {
        boolean result = scheduler.deleteJob(new JobKey(
                "printJob",      //任务名称
                "printJobGroup"  //任务所属Group
        ));
        System.out.println(result);
    }
}
