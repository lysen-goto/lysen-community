package com.lysen.repository;

import com.lysen.model.entity.DiscussPost;
import com.lysen.mapper.DiscussPostMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.SearchHit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"all"})
@SpringBootTest
public class DiscussPostRepositoryTest {
//    @Autowired
//    private DiscussPostRepository discussPostRepository;
//    @Autowired
//    private DiscussPostMapper discussPostMapper;
//
//
//    /**
//     * 修改/新增 一条数据
//     */
//    @Test
//    void save() {
//        //从数据库查询数据
//        DiscussPost discussPost = discussPostMapper.selectById(217l);
//        //放入ES
//        discussPostRepository.save(discussPost);
//    }
//
//    /**
//     * 修改/新增 多条数据
//     */
//    @Test
//    void saveAll() {
//        //从数据库查询出帖子
//        List<DiscussPost> postList = discussPostMapper.selectBatchIds(Arrays.asList(
//                1635538346346295297l,
//                1635538479574167554l,
//                1635539258322202626l,
//                1635539498488049665l,
//                1635540709396312066l
//        ));
//        //放入ES中
//        discussPostRepository.saveAll(postList);
//    }
//
//    /**
//     * 根据id删除一条数据
//     */
//    @Test
//    void deleteById() {
//        discussPostRepository.deleteById(217l);
//    }
//
//    /**
//     * 删除多条数据
//     */
//    @Test
//    void deleteAll() {
//        //根据id删除多条数据
//        discussPostRepository.deleteAllById(Arrays.asList(
//                1635538346346295297l,
//                1635538479574167554l
//        ));
//        //删除全部数据
//        discussPostRepository.deleteAll();
//    }
//
//    /**
//     * 搜索一个字段
//     */
//    @Test
//    void findByContent() {
//        List<DiscussPost> discussPosts = discussPostRepository.findByContent("欢迎你好");
//        System.out.println(discussPosts);
//    }
//
//    /**
//     * 根据多个字段搜索
//     */
//    @Test
//    void findByTitleOrContent() {
//        String keyWord = "欢迎我们 你爹";
//        List<DiscussPost> discussPostList = discussPostRepository.findByTitleOrContent(keyWord);
//        discussPostList.forEach(System.out::println);
//    }
//
//    /**
//     * 根据多个字段搜索
//     */
//    @Test
//    void findByTitleOrContentPaging() {
//        //关键词
//        String keyWord = "欢迎我们 你爹";
//        //构造分页和排序条件，如果不想排序，不加最后的参数即可
//        PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "id"));
//        //查询结果
//        Page<DiscussPost> page = discussPostRepository.findByTitleOrContent(keyWord, pageRequest);
//        //总数据数目
//        System.out.println(page.getTotalElements());
//        //页数
//        System.out.println(page.getTotalPages());
//        //结果，类型List
//        System.out.println(page.getContent());
//        //一页几个数据
//        System.out.println(page.getSize());
//    }
//
//    /**
//     * 高亮显示
//     */
//    @Test
//    void findWithHighlight() {
//        //关键词
//        String keyWord = "欢迎我们 你爹";
//        //搜索获取结果
//        List<SearchHit<DiscussPost>> searchHits = discussPostRepository.findWithHighlight(keyWord);
//        //解释一下SearchHit的方法
//        for (SearchHit<DiscussPost> hit : searchHits) {
//            //SearchHit::getContent，获取实体DiscussPost，不包含高亮显示的内容
//            DiscussPost discussPost = hit.getContent();
//            //SearchHit::getHighlightFields，获取所有高亮的信息，是一个map集合
//            Map<String, List<String>> highlightFields = hit.getHighlightFields();
//            //SearchHit::getHighlightField，获取某个字段的高亮信息，等价于hit.getHighlightFields().get("title")
//            //结果是一个List，如果title很长，且高亮的部分分散在不同的部位，则ES会将其切成若干段，实际开发往往只展示第一段
//            List<String> title = hit.getHighlightField("title");
//            List<String> content = hit.getHighlightField("content");
//        }
//        //假设最终高亮显示，只显示第一个部分
//        ArrayList<DiscussPost> result = new ArrayList<>();
//        for (SearchHit<DiscussPost> hit : searchHits) {
//            //若title有高亮部分，展示第一段
//            List<String> title = hit.getHighlightField("title");
//            if (title != null && !title.isEmpty()) {
//                hit.getContent().setTitle(title.get(0));
//            }
//            //如果content有高亮部分，展示第一段
//            List<String> content = hit.getHighlightField("content");
//            if (content != null && !content.isEmpty()) {
//                hit.getContent().setContent(content.get(0));
//            }
//            //处理后的结果放入result
//            result.add(hit.getContent());
//        }
//        //输出结果
//        result.forEach(System.out::println);
//    }
}
