package com.lysen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings({"all"})
@Component
public class RedisTest {
    @Autowired
    private RedisTemplate redisTemplate;

//    @Transactional
    public void test() {
        Object obj = redisTemplate.execute(new SessionCallback() {
            @Override
            public Object execute(RedisOperations operations) throws DataAccessException {
                operations.multi();
                operations.opsForValue().set(1, 1);
                operations.opsForValue().set(2, 2);
                return operations.exec();
            }
        });
        System.out.println(obj);
    }


}
