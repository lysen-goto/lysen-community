package com.lysen;

import com.lysen.model.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("all")
@SpringBootTest
class LysenCommunityApplicationTests {
    @Autowired
    private RedisTest redisTest;
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void test() {
        String key = "like:user:123456";
        redisTemplate.opsForValue().increment(key);
        Object result = redisTemplate.opsForValue().get(key);
        System.out.println(result);
        System.out.println(result.getClass());
    }

    @Test
    void boundKey() {
        //绑定String类型
        BoundValueOperations valueOps = redisTemplate.boundValueOps("count");
        valueOps.set(1);
        System.out.println(valueOps.get());
        //绑定Hash类型
        BoundHashOperations hashOps = redisTemplate.boundHashOps("map");
        hashOps.put("name", "Lysen");
        hashOps.put("age", 20);
        System.out.println(hashOps.get("name"));
        System.out.println(hashOps.get("age"));
        //List、Set、ZSet类似 ......
    }

    @Test
    void test2() {
        String str = "list";
        List<Integer> list = Arrays.asList(1, 2, 3, 4);
        redisTemplate.opsForValue().set(str, list);
        Object o = redisTemplate.opsForValue().get(str);
        System.out.println(o);
        System.out.println(o.getClass());
    }

    @Test
    void test3() {
        String str = "user";
        User user = new User();
        user.setUsername("Lysen");
        user.setPassword("123");
        redisTemplate.opsForValue().set(str, user);
        Object o = redisTemplate.opsForValue().get(str);
        System.out.println(o);
        System.out.println(o.getClass());
    }

}
