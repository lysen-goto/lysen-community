package com.lysen.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SuppressWarnings({"all"})
@SpringBootTest
public class DataServiceTests {
    @Autowired
    private DataService dataService;

    @Test
    void addUv() {
        //今天，共3个
//        dataService.addUniqueVisitor("8.4.12.3");
//        dataService.addUniqueVisitor("8.4.12.3");
//        dataService.addUniqueVisitor("8.4.12.3");
//        dataService.addUniqueVisitor("8.4.12.4");
//        dataService.addUniqueVisitor("8.4.12.5");

        //第二天，共5个
        dataService.addUniqueVisitor("8.4.12.5");
        dataService.addUniqueVisitor("8.4.12.6");
        dataService.addUniqueVisitor("8.4.12.7");
        dataService.addUniqueVisitor("8.4.12.8");
        dataService.addUniqueVisitor("8.4.12.9");

        //两天共7个
    }

    @Test
    void addDau() {
        //今天4个
//        dataService.addDailyActiveUser(1l);
//        dataService.addDailyActiveUser(11l);
//        dataService.addDailyActiveUser(12l);
//        dataService.addDailyActiveUser(12l);
//        dataService.addDailyActiveUser(13l);
//        dataService.addDailyActiveUser(13l);

        //明天5个
//        dataService.addDailyActiveUser(12l);
//        dataService.addDailyActiveUser(13l);
//        dataService.addDailyActiveUser(21l);
//        dataService.addDailyActiveUser(22l);
//        dataService.addDailyActiveUser(23l);

        //总共7个，因为重复2个

        dataService.addDailyActiveUser(1633996326150725633l);
        //1633996326150725633
        //4294967296
    }
}
