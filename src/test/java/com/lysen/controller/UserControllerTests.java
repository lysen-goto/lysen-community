package com.lysen.controller;

import com.baomidou.mybatisplus.core.conditions.ISqlSegment;
import com.baomidou.mybatisplus.core.conditions.segments.NormalSegmentList;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.lysen.model.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SuppressWarnings({"all"})
@SpringBootTest
public class UserControllerTests {
    public static void main(String[] args) {
        LambdaUpdateWrapper<User> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(User::getId, 1l);
        System.out.println(wrapper.getEntity());
        System.out.println(wrapper.getSqlSelect());
        System.out.println(wrapper.getSqlSet());
        System.out.println(wrapper);
        System.out.println();

        NormalSegmentList list = wrapper.getExpression().getNormal();
        for (ISqlSegment segment : list) {
            System.out.println(segment.getSqlSegment());
        }

    }
    @Test
    void test() {
    }
}
